import { BrowserRouter, Route, Routes } from 'react-router-dom';

import AppRoutes from "./Routes/appRoutes"
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <>
      <AppRoutes />
      <ToastContainer
      position="top-right" // Adjust the position as needed
      autoClose={5000} // Adjust the autoClose time as needed
      />
      </>
    );
}


export default App;
