import React from 'react';
import SidebarLayout from './cashierLayoutMain';

const CashierLayout = ({ children }) => {
  return (
      <SidebarLayout>
            {children}
        </SidebarLayout>
  );
};

export default CashierLayout;
