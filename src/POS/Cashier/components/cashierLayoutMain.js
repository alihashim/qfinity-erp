// SidebarLayout.js
import React from 'react';
import { useLocation } from 'react-router-dom';
import Sidebar from "../pages/sideBar"
// import "./sideBar.css"
const SidebarLayout = ({ children }) => {
    const location = useLocation();

    // const isSidebarEnabled = !location.pathname.stargtsWith('/'); // Decide whether to show the sidebar

    return (
        <div className="sidebar-layout">
            <div className="sidebar">
                {/* {isSidebarEnabled && <SideBar currentPath={location.pathname} />} */}
                <Sidebar/>
            </div>
            <main className="content">{children}</main>
        </div>
    );
};

export default SidebarLayout;
