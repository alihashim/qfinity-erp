import React, { useState } from 'react';
import Cake from '../assets/images/CakeImg.png';
import './itemCard.css';
import Modal from 'react-modal'; 
import CartModal from '../components/cartPopup';

const ItemCard = ({ image, name, description, price }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div className='col-lg-6 col-sm-6 p-4 mb-4 card'>
      <div className='row'>
        <div className='col-lg-3'>
          <img className='w-100' src={Cake} alt={name} />
        </div>
        <div className='col-lg-9'>
          <div className='row card-Head'>{name}</div>
          <div className='row card-subHead'>{description}</div>
          <div className='row card-Prize'>${price}</div>
          <div className='row d-flex justify-content-end'>
            <div className='col-lg-4 AddCart-btn center p-2' onClick={openModal}>
              Add to cart
            </div>
          </div>
        </div>
      </div>

      {/* Render the CartModal as a modal */}
      <Modal
        isOpen={isModalOpen}
        onRequestClose={closeModal}
        contentLabel='Cart Modal'
      >
        <CartModal />
        {/* <button onClick={closeModal}>Close</button> */}
      </Modal>
    </div>
  );
};

export default ItemCard;
