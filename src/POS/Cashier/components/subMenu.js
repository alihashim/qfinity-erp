import React from "react";
import "./SubMenu.css";

const SubMenu = ({ items, isOpen, onClose }) => {
  return (
    <div className={`dynamic-sub-menu ${isOpen ? "open" : ""}`}>
      <ul>
        {items.map((item, index) => (
          <li key={index}>{item}</li>
        ))}
      </ul>
      {/* <button onClick={onClose}>Close</button> */}
    </div>
  );
};

export default SubMenu;
