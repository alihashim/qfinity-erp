import React, { useState } from "react";
import "./sideBar.css";
import Logo from "../../assets/images/Logo.png";
import { NavLink } from "react-router-dom";
import { TfiDashboard } from "react-icons/tfi";
import { RxDashboard } from "react-icons/rx";
import { PiUsersThreeThin } from "react-icons/pi";
import { HiOutlineHome } from "react-icons/hi";
import { MdOutlineInventory2, MdOutlineInventory } from "react-icons/md";
import { TbReport } from "react-icons/tb";
import { FiSettings, FiUser, FiUsers } from "react-icons/fi";
import SubMenu from "../components/subMenu";

const SideBar = ({ id }) => {
    const [activeLink, setActiveLink] = useState(null);

    const subMenus = {
        Food: ["Sea Food", "Pizza", "Pasta","Chicken"],
        Drinks: ["Submenu 1", "Submenu 2"],
        // Add sub-menu items for other links
    };

    return (
        <div className="container-fluid">
            <div className="row flex-nowrap">
                <div className="col-auto px-sm-2 px-0 BG-Slider">
                    <div className="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
                        <a href="/" className="d-flex align-items-center pb-3 mb-md-0 me-md-auto text-white text-decoration-none txt-center">
                            <img src={Logo} className="fs-5 d-none d-sm-inline" />
                        </a>
                        <ul className="nav nav-pills sidebar-Nav flex-column mb-sm-auto mb-0 align-items-center" id="menu">
                            <li className="nav-item">
                                <div className="text-center py-lg-1">
                                    <NavLink
                                        // to="/Home"
                                        className="nav-link align-middle"
                                        activeClassName="active"
                                        onClick={() => setActiveLink("Food")}
                                    >
                                        <TfiDashboard size={40} className="" />
                                        <a className="nav-link align-middle">
                                            <h3 className="d-none d-sm-inline main-txt" activeClassName="active">
                                                Foods
                                            </h3>
                                        </a>
                                    </NavLink>
                                    {activeLink === "Food" && (
                                        <SubMenu
                                            items={subMenus.Food}
                                            isOpen={activeLink === "Food"}
                                            onClose={() => setActiveLink(null)}
                                        />
                                    )}
                                </div>
                            </li>
                            <li className="nav-item">
                                <div className="text-center py-lg-1">
                                    <NavLink
                                        // to="/Home"
                                        className="nav-link align-middle"
                                        activeClassName="active"
                                        onClick={() => setActiveLink("Drinks")}
                                    >
                                        <TfiDashboard size={40} className="" />
                                        <a className="nav-link align-middle">
                                            <h3 className="d-none d-sm-inline main-txt" activeClassName="active">
                                                Drinks
                                            </h3>
                                        </a>
                                    </NavLink>
                                    {activeLink === "Drinks" && (
                                        <SubMenu
                                            items={subMenus.Drinks}
                                            isOpen={activeLink === "Drinks"}
                                            onClose={() => setActiveLink(null)}
                                        />
                                    )}
                                </div>
                            </li>
                        </ul>
                        <hr />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SideBar;
