import React, { useState } from 'react';
import ItemCard from '../components/itemCard';
import "./cashierMenu.css"
import { RiSecurePaymentFill } from 'react-icons/ri';
import { MdHistoryEdu } from 'react-icons/md';
import { PiPlaceholderDuotone } from 'react-icons/pi';
const items = [
  {
    id: 1,
    image: 'URL_to_image1',
    name: 'Chicken Sandwich',
    description: 'A small river named Duden flows by their place.',
    price: 25.00,
  },
  {
    id: 2,
    image: 'URL_to_image2',
    name: 'Burger',
    description: 'Another description for the second item.',
    price: 15.00,
  },
  {
    id: 3,
    image: 'URL_to_image2',
    name: 'Burger',
    description: 'Another description for the second item.',
    price: 15.00,
  },
  {
    id: 4,
    image: 'URL_to_image2',
    name: 'Burger',
    description: 'Another description for the second item.',
    price: 15.00,
  },
  {
    id: 5,
    image: 'URL_to_image2',
    name: 'Burger',
    description: 'Another description for the second item.',
    price: 15.00,
  },
  {
    id: 6,
    image: 'URL_to_image2',
    name: 'Burger',
    description: 'Another description for the second item.',
    price: 15.00,
  },
  // Add more items as needed
];

function CashierMenu() {
  const [activeView, setActiveView] = useState('Payment'); // Initialize active view

  return (
    <>
      <div className="row">
        <div className="Heads pt-3 pl-3">Items Menu </div>
      </div>
      <div className="row">
        <div className="SilverHeads">Here is your item list!</div>
      </div>
    <div className='row p-5'>
      <div className='col-lg-8'>
        <div className="row">
          {items.map((item) => (
            <ItemCard
              key={item.id}
              image={item.image}
              name={item.name}
              description={item.description}
              price={item.price}
            />
          ))}
        </div>
      </div>
      <div className='col-lg-4'>
        <div className='navbar'>
          <ul className='nav nav-tabs' role='tablist'>
            <li
              className={`nav-item ${activeView === 'Payment' ? 'head-active' : ''}`}
              onClick={() => setActiveView('Payment')}
            >
              <div className='d-flex '>

                <RiSecurePaymentFill className='icon-size' />

                Payment
              </div>
            </li>
            <li
              className={`nav-item ${activeView === 'Place Order' ? 'head-active' : 'normal-head'}`}
              onClick={() => setActiveView('Place Order')}
            >
              <div className='d-flex align-content-center '>

                <PiPlaceholderDuotone className='icon-size' />
                Place 
              </div>

            </li>
            <li
              className={`nav-item ${activeView === 'History' ? 'head-active' : ''}`}
              onClick={() => setActiveView('History')}
            >
              <div className='d-flex '>
                <MdHistoryEdu className='icon-size' />
                History
              </div>
            </li>
          </ul>
        </div>

        {/* Content based on the selected view */}
        {activeView === 'Payment' && (
          <div>

            <div className='payment_bar p-3'>
              <div className='row head_c'>

                New Order
              </div>
              <div>
                New Order
              </div>

            </div>
          </div>

          /* Render payment content here */
        )}

        {activeView === 'Place Order' && (
          /* Render place order content here */
          <div>
            Place 
          </div>
        )}

        {activeView === 'History' && (
          /* Render history content here */
          <div>
            Histort
          </div>
        )}
      </div>
    </div>
    </>

  );
}

export default CashierMenu;
