// OrderCard.js
import React from 'react';
import { useDrag } from 'react-dnd';

const OrderCard = ({ order, onDrop }) => {
    const [{ isDragging }, drag] = useDrag({
        type: 'ORDER',
        item: { id: order.id },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
    });
    debugger
    return (
        <div ref={drag} style={{ opacity: isDragging ? 0.5 : 1, border: '1px solid', padding: '8px', marginBottom: '8px' }}        >
            <div className='d-flex justify-content-between'>
                <div>Type :</div>
                <div>{order?.orderType}</div>
            </div>
            <div className='d-flex justify-content-between'>
                <div>Customer :</div>
                <div>{order?.customerName}</div>
            </div>
            <div className='d-flex justify-content-between'>
                <div>Created at :</div>
                <div>{order?.date}</div>
            </div>
            {/* item */}
            {order?.orderItems?.map((val) => {
                return (
                    <div className=''>
                        <div>{val?.itemName}</div>
                        <div className='d-flex gap-2'>
                            <div>qty : {val?.quantity}</div>
                            <div>{val?.kitchenInstructions}</div>
                            {val?.addOns?.map((addson) => {
                                return (
                                    <div>{addson?.name}</div>
                                )
                            })}
                        </div>
                    </div>
                )
            })}
        </div>
    );
};

export default OrderCard;
