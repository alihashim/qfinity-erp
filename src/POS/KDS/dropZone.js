// OrderSection.js
import React from 'react';
import { useDrop } from 'react-dnd';
import "./kitchen.css"
const OrderSection = ({ status, onDrop, children }) => {
    const [{ canDrop, isOver }, drop] = useDrop({
        accept: 'ORDER',
        drop: (item) => onDrop(item.id, status),
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
        }),
    });

    const isActive = canDrop && isOver;

    return (
        <div ref={drop} className='section-contain'>
            <div className='status-contain'>
                {status}
            </div>
            <div className='pt-4'>
                {children}
            </div>
        </div>
    );
};

export default OrderSection;
