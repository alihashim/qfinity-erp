import React, { useState, useEffect } from 'react';
import CookHeader from './kitchenHeader';
import Select from 'react-select';
import "./kitchen.css"
import { ReactComponent as Refresh } from '../assets/images/svgIcons/Refresh.svg';
import OrderSection from './dropZone';
import OrderCard from './Drag';
import ApiService from '../services/apiService';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

const KitchenDisplay = ({ }) => {
    const [selected, setSelected] = useState(null);
    const [orders, setOrders] = useState([]);
    const [draggedOrder, setDraggedOrder] = useState(null);

    const filterValue = [
        { value: '0', label: 'Display All Orders' },
        { value: '1', label: 'By Order Type : Pickup' },
        { value: '2', label: 'By Order Type : Delivery' },
        { value: '3', label: 'By Order Type : Dinning' },
    ];

    const handleTypeSelect = (e) => {
        setSelected(e.value);
    }
    useEffect(() => {
        GetOrders();
    }, []);

    const GetOrders = async () => {
        const apiService = new ApiService();
        let res = await apiService.get("GET_ORDERS");

        const mappedOrders = res?.data?.result?.map(order => ({
            ...order,
            status: mapApiStatusToEnum(order.orderStatus),
        }));
        debugger
        setOrders(mappedOrders);
    }
    const mapApiStatusToEnum = (apiStatus) => {
        switch (apiStatus) {
            case 'Accepted':
                return OrderStatus.Accepted;
            case 'Cooking':
                return OrderStatus.Cooking;
            // Add more cases as needed for other statuses
            default:
                return OrderStatus.OrderInQueue; // Default to OrderInQueue if not matched
        }
    };
    const handleDrop = async (orderId, newStatus) => {

        const statusMap = {
            OrderInQueue: OrderStatus.OrderInQueue,
            Accepted: OrderStatus.Accepted,
            Cooking: OrderStatus.Cooking,
            OnTheWay: OrderStatus.OnTheWay,
            // Completed: OrderStatus.Completed,
        };

        const updatedOrders = orders.map((order) =>
            order.id === orderId ? { ...order, status: statusMap[newStatus] } : order
        );

        setOrders(updatedOrders);
        setDraggedOrder(null);

        let body = {
            id: orderId,
            orderStatus: statusMap[newStatus]
        }
        const apiService = new ApiService();
        const response = await apiService.put('ORDER_STATUS', body);

    };
    const columnStyles = {
        display: 'grid',
        gridTemplateColumns: 'repeat(4, 1fr)',
        gap: '16px',
    };

    return (
        <div className="">
            <div><CookHeader /></div>
            <div className='bghead d-flex justify-content-between px-lg-5' >
                <div className='col-3'>
                    <Select
                        defaultValue={selected}
                        options={filterValue}
                        onChange={handleTypeSelect}
                        value={filterValue.find(function (option) {
                            return option?.value === selected;
                        })}
                        className='reactselect'
                    />
                </div>
                <div className="d-flex justify-content-end">
                    <div className='refbtn'>
                        <Refresh />
                        Reload Orders
                    </div>
                </div>
            </div>
            <div>
                <DndProvider backend={HTML5Backend}>
                    <div>
                        <div style={columnStyles}>
                            {Object.keys(OrderStatus).map((statusKey) => (
                                <OrderSection key={statusKey} status={statusKey} onDrop={handleDrop}>
                                    {orders
                                        .filter((order) => order.status === OrderStatus[statusKey])
                                        .map((order) => (
                                            <OrderCard key={order.id} order={order} onDrop={handleDrop} />
                                        ))}
                                </OrderSection>
                            ))}
                        </div>

                        {/* <div style={{ display: 'flex', marginTop: '16px' }}>
                            {orders.map((order) => (
                                <OrderCard
                                    key={order.id}
                                    order={order}
                                    onDrop={() => setDraggedOrder(order)}
                                />
                            ))}
                        </div> */}
                    </div>
                </DndProvider>
            </div>
        </div>
    );
};

export default KitchenDisplay;

export const OrderStatus = {
    OrderInQueue: 1,
    Accepted: 2,
    Cooking: 3,
    OnTheWay: 4,
    Completed: 5,
};