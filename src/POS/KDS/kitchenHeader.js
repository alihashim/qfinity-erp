import { React, useState, useEffect } from 'react'
import Profile from "../assets/images/profileImg.png"
import Logo from "../assets/images/Qfinitylogos.png"
import "./kitchenheader.css"

const CookHeader = () => {

    return (
        <nav className="navbar-expand-lg bg">
            <div class="container-fluid px-lg-5">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarSupportedContent">
                    <div>
                        <img src={Logo} className=''/>
                    </div>
                    <div className='d-flex justify-content-end end'>
                        <div className='profilebox'>
                            Asad,Khan
                        </div>
                        <div className='KDCprofileimgbox '>
                            <img src={Profile} alt='profile' />
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default CookHeader