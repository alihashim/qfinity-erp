import React, { useState } from "react";
import { Navbar, Button, NavbarToggler } from "reactstrap";
import {HiChevronLeft} from "react-icons/hi"
const Topbar = ({ toggleSidebar }) => {
  const [topbarIsOpen, setTopbarOpen] = useState(true);
  const toggleTopbar = () => setTopbarOpen(!topbarIsOpen);

  return (
    <Navbar color="light" className=" bg-white p-3" expand="md">
      <Button color="info" onClick={toggleTopbar}>
        {/* <FontAwesomeIcon icon={faAlignLeft} /> */}
        <h3><HiChevronLeft/></h3>
      </Button>
    </Navbar>
  );
};

export default Topbar;
