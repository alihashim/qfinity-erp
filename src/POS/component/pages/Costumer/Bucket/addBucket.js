import { useState, React } from 'react'
import "./bucket.css"
import Modal from 'react-modal'; // Import the Modal component
import CartModal from '../popup/modifier'; // Import your CartModal component
import { ReactComponent as Delete } from '../../../../assets/images/svgIcons/Delete.svg';
import { ReactComponent as Add } from '../../../../assets/images/svgIcons/Add_round_light.svg';
import { useNavigate } from 'react-router-dom'

const Bucket = ({ cartItems, removeFromCart, updateQuantity }) => {

    
    const handleDelete = (index) => {
        removeFromCart(index);
    };

    const handleQuantityChange = (index, newQuantity) => {
        const updatedQuantity = Math.max(newQuantity, 1);
        updateQuantity(index, updatedQuantity);
    };

    let navigate = useNavigate();

    const handlePayment = () => {
        navigate('/costumer/menu/Cart/Payment', { state: { cartItems } });
    }
    return (
        <div >
            <div className='pt-lg-5'>
                <div className='cartBG'>
                    Your Item Cart
                </div>
                <div className='table-responsive'>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Modifier</th>
                                <th>Quantity</th>
                                <th>Rate</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {cartItems?.map((item, index) => (
                                <tr key={index}>
                                    <td >{item?.itemData?.name}</td>
                                    <td>{item?.selectedAdOn?.name}</td>
                                    <td>
                                        <div className="quantity-control d-flex">
                                            <div onClick={() => handleQuantityChange(index, item?.quantity - 1)} className='addbtn'>-</div>
                                            <span className='px-2 center'>{item.quantity}</span>
                                            <div onClick={() => handleQuantityChange(index, item?.quantity + 1)} className='addbtn'>+</div>
                                        </div>
                                    </td>
                                    <td>$ {item?.itemData?.price + item?.selectedAdOn?.price}</td>
                                    <td>
                                        <Delete onClick={() => handleDelete(index)} />
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
                <div className='center'> <button onClick={handlePayment} className='AddBtn w-100'>Checkout</button></div>
            </div>
        </div>
    )
}

export default Bucket