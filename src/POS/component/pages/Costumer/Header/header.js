import React from 'react'
import { ReactComponent as Logo } from '../../../../assets/images/svgIcons/logo.svg';
import Profile from "../../../../assets/images/profileImg.png"
import "./header.css"
const CostumerHeader = () => {

    return (
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
            <div class="container-fluid px-lg-5">
                <a class="navbar-brand" href="/">
                    {/* <img src={Logo} alt="logo" /> */}
                    <Logo />
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0 gap-5 px-lg-5">
                        <li class="nav-item">
                            <a class="nav-link nav-active" aria-current="page" href="#">Menu</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Offers</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href='#'>Combos</a>
                        </li>
                    </ul>
                    {/* <div className='d-flex justify-content-end mbl-display'>
                        <div className='profilebox'>
                            Asad,Khan
                        </div>
                        <div className='profileimgbox'>
                            <img src={Profile} alt='profile' />
                        </div>
                    </div> */}
                </div>
            </div>
        </nav>
    )
}

export default CostumerHeader