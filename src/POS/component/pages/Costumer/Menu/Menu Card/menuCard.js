import { React, useState } from 'react';
import './menuCard.css'
import { useNavigate } from 'react-router-dom';
import 'bootstrap'
import Pizza from "../../../../../assets/images/P1.png"
import CartModal from "../../popup/modifier"
import Modal from 'react-modal'; // Import the Modal component

const CostumerMenuCard = ({ val, addToCart }) => {

    
    let navigate = useNavigate();

    // const handleATC = () => {
    //     addToCart(val);
    //     navigate('/costumer/menu');
    // };

    const [isModalOpen, setIsModalOpen] = useState(false);
    const [CartValue, setCartValue] = useState("");

    const openModal = (e) => {
        setCartValue(e)
        setIsModalOpen(true);
    }
    const closeModal = () => {
        setIsModalOpen(false);
    };

    return (
        <div className="col-lg-3 col-md-4 col-sm-6">
            <div className="menu-item-card my-5">
                <div className="menu-item-header p-0">
                    {val?.Discount && (
                        <div className="tagfont">
                            {val?.Discount} Off
                        </div>
                    )}
                    {val?.Delivery && (
                        <div className="tagfont min-top">
                            {val?.Delivery}
                        </div>
                    )}
                </div>
                <div className="menu-item-details justify-content-center IMG-BG">
                    <div><img src={Pizza} className='item-pic' /></div>
                </div>
                <div className="main-color p-lg-3">
                    <div className="ItemName">{val?.name}</div>
                    <div className="item-des mt-1">{val?.description}</div>
                    <div className="d-flex center gap-3 mt-1">
                        {/* <div className="text-decoration-line-through item-price">$ {val?.price}</div> */}
                        <div className="item-price ">
                            $   {val?.price}
                        </div>
                    </div>
                    <div className="row p-1 mt-3 center">
                        <button className="edit-button p-2 w-75" onClick={() => openModal(val)}>
                            Add to cart
                        </button>
                    </div>
                </div>
            </div>
            <Modal
                isOpen={isModalOpen}
                onRequestClose={closeModal}
                contentLabel='Cart Modal'
                value={CartValue}
            >
                <CartModal CartValue={CartValue} closeModal={closeModal} addToCart={addToCart} />
                {/* <button onClick={closeModal}>Close</button> */}
            </Modal>
        </div>
    );
};

export default CostumerMenuCard;
