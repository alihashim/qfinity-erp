import { React, useEffect, useState } from 'react'
import CostumerHeader from '../Header/header'
import "./costumer.css"
import CostumerNavBar from './NavBar/navBar';
import CostumerMenuCard from './Menu Card/menuCard';
// import menu from './dummyData';
import CostumerBanner from '../Banners/banner';
import SearchNav from './searchNav';
import Bucket from '../Bucket/addBucket';
import ApiService from '../../../../services/apiService';
const CustomerMenu = () => {

    const [cartItems, setCartItems] = useState([]);
    const [menu, setmenu] = useState([]);

    useEffect(() => {
        GetMenu();
    }, []);

    const addToCart = (item) => {
        
        const existingItem = cartItems.find((cartItem) => cartItem?.id === item?.itemData?.id);
        
        if (existingItem) {
            // If the item already exists, update its quantity
            const updatedCartItems = cartItems.map((cartItem) =>
                cartItem.id === item?.itemData?.id ? { ...cartItem, quantity: cartItem.quantity + 1 } : cartItem
            );
            setCartItems(updatedCartItems);
            
        } else {
            // If the item doesn't exist, add it to the cart
            setCartItems([...cartItems, { ...item, quantity: 1 }]);
            
        }
    };
    
    const removeFromCart = (index) => {
        const newCartItems = [...cartItems];
        newCartItems.splice(index, 1);
        setCartItems(newCartItems);
    };

    const updateQuantity = (index, newQuantity) => {
        const updatedCartItems = [...cartItems];
        updatedCartItems[index].quantity = newQuantity;
        setCartItems(updatedCartItems);
    };


    const GetMenu = async () => {
        const apiService = new ApiService();
        let res = await apiService.get("GET_MENU");
        setmenu(res.data.result)
    }
    return (
        <div className="">
            <CostumerHeader />
            <div className='container-fluid m-0 p-0 pt-5 px-5' >
                <SearchNav />
                <div className="pt-5">
                    <CostumerNavBar />
                </div>
            </div>
            {/* <div className='px-5 pt-5 p-0'>
                <CostumerBanner />
            </div> */}
            <div className="px-5">
                <div className="row">
                    <div className={cartItems.length > 0 ? 'col-9' : 'col-12'}>
                        <div className="row">
                            {menu.map((val, key) => (
                                <CostumerMenuCard key={key} val={val} addToCart={addToCart} />
                            ))}
                        </div>
                    </div>
                    {cartItems.length > 0 && (
                        <div className="col-3">
                            <Bucket cartItems={cartItems} removeFromCart={removeFromCart} updateQuantity={updateQuantity} />
                        </div>
                    )}
                </div>
            </div>
            {/* <SideBar /> */}
        </div>
    )
}

export default CustomerMenu