import React from 'react'
import { ReactComponent as Location } from '../../../../assets/images/svgIcons/Location.svg';
import "./costumer.css"
const SearchNav = () => {

    return (
        <div className='d-flex justify-content-between inline'>
            <input type="search" class="form-control w-25 p-2" placeholder="Search" />
            <div className='d-flex justify-content-end pad-t flx-strt-mbl'>
                <div className='locbox'>
                    <h3 className='Delivr'>Delivering to</h3>
                    <h4 className='delivrloc'>Satellite Town D-Block</h4>
                </div>
                <div className='locationbox'>
                    <Location />
                </div>
            </div>
        </div>
    )
}

export default SearchNav