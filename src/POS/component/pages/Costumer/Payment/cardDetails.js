import React, { useState, useEffect } from 'react';
import CostumerHeader from '../Header/header';
import axios from 'axios';
import { useNavigate, useLocation } from 'react-router-dom';
import { toast } from 'react-toastify';
import { loadStripe } from '@stripe/stripe-js';
import ApiService from '../../../../services/apiService';

import './payment.css';
import Item2 from '../../../../assets/images/P1.png';
const stripePromise = loadStripe('pk_test_51Kku1CLBbHIb8JaUQmSNv3YkbJHrbH1u2GaA2mkAOChyuEVYFAVHi7UotKNDnt2cSBBsYz9ebgndtPdkoXToInXP00fLEPzoyA'); // Replace with your actual publishable key

const CardDetails = () => {
    let navigate = useNavigate();
    const location = useLocation();
    const { state } = location;
    const data = state?.cartItems;
    const [cardDetails, setCardDetails] = useState({
        cardOwner: '',
        cardNumber: '',
        expirationMonth: '',
        expirationYear: '',
        cvc: '',
        saveCardDetails: false,
    });

    const handleInputChange = (e) => {
        const { name, value, type, checked } = e.target;
        setCardDetails((prevDetails) => ({
            ...prevDetails,
            [name]: type === 'checkbox' ? checked : value,
        }));
    };

    useEffect(() => {
        const initStripe = async () => {
            const stripe = await stripePromise;
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#card-element');
        };

        initStripe();
    }, [stripePromise]);


    // const handlePayment = async () => {
    //     const stripe = await stripePromise;

    //     try {
    //         const response = await axios.post('/api/create-payment-intent', {
    //             items: data,
    //         });

    //         const { clientSecret } = response.data;

    //         const result = await stripe.confirmCardPayment(clientSecret, {
    //             payment_method: {
    //                 card: stripe.elements.getElement('card'), // Use stripe.elements.getElement
    //                 billing_details: {
    //                     name: cardDetails.cardOwner,
    //                 },
    //             },
    //         });

    //         if (result.error) {
    //             console.error(result.error.message);
    //             toast.error(result.error.message, {
    //                 position: 'top-right',
    //                 autoClose: 5000,
    //             });
    //         } else {
    //             // ... rest of your code ...
    //         }
    //     } catch (error) {
    //         console.error(error);
    //         toast.error('Something went wrong. Please try again.', {
    //             position: 'top-right',
    //             autoClose: 5000,
    //         });
    //     }
    // };

    const handlePayment = async () => {
        try {
            const totalOrderAmount = data.reduce((total, item) => total + item.itemData.price, 0);
            const datareq = {
                date: "2023-11-18T09:19:29.804Z",
                customerName: cardDetails?.cardOwner,
                orderType: "Dine-In",
                orderSource: "--",
                orderAmount: totalOrderAmount,
                tax: 1,
                orderStatus: "Accepted",
                totalDiscount: 0,
                amountCollected: totalOrderAmount,
                amountReturned: 0,
                orderItems: data?.map(item => ({
                    itemName: item?.itemData?.name,
                    amount: item?.itemData?.price,
                    itemDiscount: 0,  // You may need to calculate this based on your business logic
                    quantity: item?.quantity,
                    kitchenInstructions: item?.specialInstructions,
                    addOns: [
                        {
                            name: item?.selectedAdOn?.name,
                            price: item?.selectedAdOn?.price
                        }
                    ]
                }))
            };
            const apiService = new ApiService();

            let res = await apiService.post("CREATE_ORDER", datareq);
            if (res?.data?.statusCode === 201) {
                // Show success toast
                navigate('/costumer/wait');
                toast.success("Order Created Successfully");
            }
            else {
                toast.warning('Waiting for Order');
            }
        } catch (error) {
            console.error('Error:', error);
        }
    };


    return (
        <div className=''>
            <CostumerHeader />
            <div className='px-lg-5'>
                <div className='px-lg-5'>
                    <div className='row p-lg-5'>
                        <div className='col-lg-6 p-lg-5'>
                            <div className='headTxt'>Payment</div>
                            <div className='lineBreak py-lg-3'></div>
                            <div className='bodytxt pt-3'>Pay with:</div>
                            <div className='d-flex py-3'>
                                <div className=''>
                                    <input
                                        className='form-check-input'
                                        type='radio'
                                        name='flexRadioDisabled'
                                        id='Card'
                                        checked
                                        style={{ width: '20px', height: '20px' }}
                                    />
                                    <label className='form-check-label checkItem px-3' htmlFor='Card'>
                                        Card
                                    </label>
                                </div>
                            </div>
                            <div className=''>
                                <label data-toggle='tooltip' title='Three digit CV code on the back of your card'>
                                    <h6>
                                        CVV <i className='fa fa-question-circle d-inline'></i>
                                    </h6>
                                </label>
                                <div id='card-element' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='username'>
                                    <h6>Card Owner</h6>
                                </label>
                                <input
                                    type='text'
                                    name='cardOwner'
                                    placeholder='Card Owner Name'
                                    className='form-control-Payment w-100'
                                    onChange={handleInputChange}
                                />
                            </div>
                            <div className='form-group py-4'>
                                <label htmlFor='cardNumber'>
                                    <h6>Card number</h6>
                                </label>
                                <div className='input-group'>
                                    <input
                                        type='text'
                                        name='cardNumber'
                                        placeholder='Valid card number'
                                        className='form-control-Payment w-100'
                                        onChange={handleInputChange}
                                    />
                                </div>
                            </div>
                            <div className='row'>
                                <div className='col-sm-8'>
                                    <div className='form-group'>
                                        <label>
                                            <span className='hidden-xs'>
                                                <h6>Expiration Date</h6>
                                            </span>
                                        </label>
                                        <div className='input-group'>
                                            <input
                                                type='number'
                                                placeholder='MM'
                                                name='expirationMonth'
                                                className='form-control-Payment'
                                                onChange={handleInputChange}
                                            />
                                            <input
                                                type='number'
                                                placeholder='YY'
                                                name='expirationYear'
                                                className='form-control-Payment'
                                                onChange={handleInputChange}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className='col-sm-4'>
                                    <div className='form-group mb-4'>
                                        {' '}
                                        <label data-toggle='tooltip' title='Three digit CV code on the back of your card'>
                                            <h6>
                                                CVV <i className='fa fa-question-circle d-inline'></i>
                                            </h6>
                                        </label>{' '}
                                        <input
                                            type='text'
                                            name='cvv'
                                            className='form-control-Payment'
                                            onChange={handleInputChange}
                                        />{' '}
                                    </div>
                                </div>
                            </div>
                            <div className='d-flex gap-3'>
                                <input
                                    className='form-check-input'
                                    type='checkbox'
                                    name='flexRadioDisabled'
                                    id='save'
                                    checked={cardDetails.saveCardDetails}
                                    style={{ width: '16px', height: '16px' }}
                                    onChange={handleInputChange}
                                />
                                <h6 className='form-check-label' htmlFor='save'>
                                    Save card details
                                </h6>
                            </div>
                            <button className='w-100 AddBtn' onClick={handlePayment}>
                                Pay USD59.28
                            </button>
                            <div className='descrip py-4'>
                                Your personal data will be used to process your order, support your experience throughout this website,
                                and for other purposes described in our privacy policy.
                            </div>
                        </div>
                        <div className='col-lg-6 p-lg-5'>
                            <div className='headTxt'>Order Summary</div>
                            <div className='lineBreak py-lg-3'></div>
                            {data?.map((val, key) => (
                                <div className='row pt-4' key={key}>
                                    <div className='col-lg-2'>
                                        <img src={Item2} alt='Item' className='itmImg' />
                                    </div>
                                    <div className='col-lg-8'>
                                        <div className='bodytxt'>{val?.itemData?.name || 'Pizza'}</div>
                                        <div className='bodytxt mainclr'>Qty: {val?.quantity || '2'}</div>
                                    </div>
                                    <div className='bodytxt col-2'>$ {val?.itemData?.price || '250'}</div>
                                </div>
                            ))}
                            <div className='lineBreak py-lg-3'></div>
                            <div className='form-group pt-5'>
                                <div className='input-group gap-5'>
                                    <input
                                        type='text'
                                        name='cardNumber'
                                        placeholder='Gift or discount code'
                                        className='form-control-Payment w-75'
                                        required
                                    />
                                    <button type='button' className='aplybtn'>
                                        Apply
                                    </button>
                                </div>
                            </div>
                            <div className='lineBreak py-lg-4'></div>
                            <div className='d-flex justify-content-between py-4'>
                                <div className='bodytxt'>Subtotal</div>
                                <div className='bodytxt'>$49.80</div>
                            </div>
                            <div className='d-flex justify-content-between'>
                                <div className='bodytxt'>Shipping</div>
                                <div className='bodytxt'>$4.80</div>
                            </div>
                            <div className='lineBreak py-lg-4'></div>
                            <div className='d-flex justify-content-between pt-lg-4'>
                                <div>
                                    <div className='bodytxt'>Total</div>
                                    <div className='mainclr'>Including $2.24 in taxes</div>
                                </div>
                                <div className='totalprice'>$ 54.20</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CardDetails;
