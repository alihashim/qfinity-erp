import React from 'react'
import { useState } from 'react'
import CartSlider from '../Cart/imgSlider'
import "./modifier.css"
import { AiOutlinePlus } from "react-icons/ai"
import { RiSubtractFill } from "react-icons/ri"
import pizza from "../../../../assets/images/P1.png"
import { useNavigate } from 'react-router-dom'
const ModifierModal = ({ CartValue, closeModal, addToCart }) => {
    let itemData = CartValue
    let AdOn = CartValue?.addOns
    let category = CartValue?.category
    let navigate = useNavigate();

    const [selectedAdOn, setSelectedAdOn] = useState(null);
    const [specialInstructions, setSpecialInstructions] = useState('');
    

    const handleRadioChange = (item) => {
        setSelectedAdOn(item);
    };

    const handleSpecialInstructionsChange = (event) => {
        setSpecialInstructions(event.target.value);
    };

    const handleATC = () => {
        const mergedData = {
            selectedAdOn,
            specialInstructions,
            itemData,
        };
        addToCart(mergedData);
        navigate('/costumer/menu');
        closeModal(true);
    };
    return (
        <div className=''>
            <div className='container-fluid m-0 p-0 pt-5 px-5' >
                <div className="row py-lg-3">
                    <div className="col-lg-4">
                        <CartSlider val={itemData} />
                    </div>
                    <div className="col-lg-8 px-lg-5">
                        <div className="CartName">{itemData?.name}</div>
                        <div className="cartPrice pt-2">$  {itemData?.price}</div>
                        <div className="py-lg-5">
                            {AdOn?.map((item) => (
                                <div key={item.id}>
                                    <div className="form-check d-flex justify-content-between pb-3">
                                        <div className="gap-3 d-flex">
                                            <input
                                                className="form-check-input"
                                                type="radio"
                                                name="flexRadioDisabled"
                                                id={item.id}
                                                checked={selectedAdOn === item}
                                                onChange={() => handleRadioChange(item)}
                                                style={{ width: "26.81px", height: "26.81px" }}
                                            />
                                            <label className="form-check-label checkItem" htmlFor={item.id}>
                                                {item?.name}
                                            </label>
                                        </div>
                                        <div className="checkItem">$ {item?.price}</div>
                                    </div>
                                    <div className="lineBreak"></div>
                                </div>
                            ))}
                            <div className="form-group">
                                <label className="special pb-3">Special Instructions</label>
                                <br />
                                <textarea
                                    rows="4"
                                    cols="100"
                                    className="textarea"
                                    name="Description"
                                    form="usrform"
                                    placeholder="Enter the instructions about this item if any."
                                    value={specialInstructions}
                                    onChange={handleSpecialInstructionsChange}
                                ></textarea>
                            </div>
                            <div className='row pt-lg-5'>
                                <div className='col-lg-12'>
                                    <button onClick={handleATC} className='w-100 AddBtn'> Add to cart </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ModifierModal