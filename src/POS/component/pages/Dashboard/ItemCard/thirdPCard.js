import React from 'react';
import "./thirdP.css"
const ThirdPartyCard = ({ val }) => {

    return (
        <div className=' col-lg-3'>
            <div className='brder  d-flex p-lg-4 gap-4'>
                <div>
                    <img src={val?.logo} />
                </div>
                <div>
                    <div className='income'>{val?.income}</div>
                    <div className='ptyName'>{val?.Name}</div>
                </div>
            </div>
        </div>
    );
};

export default ThirdPartyCard;
