import React from 'react';

const StockCard = ({ val }) => {

    return (
        <div className='p-2'>
            <div className='d-flex justify-content-between'>
                <div className='col-lg-2'><img src={val?.logo} className='itmimg'/></div>
                <div className='col-lg-7'>
                    <div>{val?.Name}</div>
                    <div class="progress">
                        <div className="progress-bar" role="progressbar" aria-valuenow={val?.income} aria-valuemin="0" aria-valuemax="800" ></div>
                    </div>
                </div>
                <div className='d-flex gap-1 col-lg-3 center'>
                    <div>{val?.income}</div>/<div>{val?.dailytarget}</div>
                </div>
            </div>
        </div>
    );
};

export default StockCard;
