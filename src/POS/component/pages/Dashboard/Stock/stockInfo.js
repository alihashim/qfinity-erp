import React from 'react';
import StockCard from './stockCard';
import { stock } from '../dummyData';

const StockInformation = ({ val }) => {

    return (
        <div className='dashcard'>
            <div>
                <div className='chatrtHead'>Stock Information</div>
                <div className='charthead2'>Lorem ipsum dolor sit amet, consectetur</div>
            </div>
            <div>
                {stock?.map((val, key) => (
                    <StockCard val={val} />
                ))}
            </div>
        </div>
    );
};

export default StockInformation;
