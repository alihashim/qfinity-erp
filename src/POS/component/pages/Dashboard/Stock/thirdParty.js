import React from 'react';
import StockCard from './stockCard';
import { third } from '../dummyData';

const ThirdParty = ({ val }) => {

    return (
        <div className='dashcard'>
            <div>
                <div className='chatrtHead'>Delivery Sources</div>
                <div className='charthead2'>Lorem ipsum dolor sit amet, consectetur</div>
            </div>
            <div>
                {third?.map((val, key) => (
                    <StockCard val={val} />
                ))}
            </div>
        </div>
    );
};

export default ThirdParty;
