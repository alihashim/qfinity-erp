import React from 'react';
import AdminHeader from '../header/adminHeader';
import Filter from './filter';
import "./dashboard.css"
import { thirdParty, Revenue } from './dummyData';
import ThirdPartyCard from './ItemCard/thirdPCard';
import CustomBarChart from '../charts/barChart';
import StockInformation from './Stock/stockInfo';
import OrderSummery from './orderSum/orderSum';
import LineChart from '../charts/lineChart';
import ThirdParty from './Stock/thirdParty';

const Dashboard = () => {


    return (
        <div className='p-lg-5'>
            {/* <AdminHeader /> */}
            <div className='p-lg-5'>
                <div className="trdptybox row p-lg-5 d-flex justify-content-between">
                    {thirdParty?.map((val, key) => (
                        <ThirdPartyCard val={val} />
                    ))}
                </div>
                <div className='py-lg-5'>
                    <div className='trdptybox row p-lg-5 d-flex justify-content-between'>
                        {Revenue?.map((val, key) => (
                            <ThirdPartyCard val={val} />
                        ))}
                    </div>
                </div>
                <div className='row justify-content-between'>
                    <div className='col-6'>
                        <CustomBarChart />
                    </div>
                    <div className="col-6">
                        <OrderSummery />
                    </div>
                </div>
                <div className='row py-5'>
                    <div className='col-6'>
                        <div className='row' >
                            <div className='col-6'>
                                <StockInformation />
                            </div>
                            <div className='col-6'>
                                <ThirdParty />
                            </div>
                        </div>
                    </div>
                    <div className="col-6">
                        <div className='dashcard'>
                            <div className='pb-2'>
                                <div className='chatrtHead'>Sales by Delivery Sources</div>
                                <div className='charthead2'>$ 550052</div>
                            </div>
                            <LineChart />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Dashboard;
