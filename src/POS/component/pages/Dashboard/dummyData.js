import Uber from "../../../assets/images/Logos/UberEats.png"
import DoorDash from "../../../assets/images/Logos/doordash.png"
import FoodPanda from "../../../assets/images/Logos/foodpanda.png"
import JustEat from "../../../assets/images/Logos/justeats.png"
import cup from"../../../assets/images/Cup.png"
import Customer from"../../../assets/images/Customer.png"
import Orders from"../../../assets/images/Orders.png"
import Dollar from"../../../assets/images/Revenue.png"
import item from"../../../assets/images/C-1.png"
import item1 from"../../../assets/images/C-2.png"
import item2 from"../../../assets/images/C-3.png"
import item3 from"../../../assets/images/C-4.png"
import item5 from"../../../assets/images/Item-2.png"
export const thirdParty = [
    {
        logo: Uber,
        Id: 1,
        income: 650,
        Name: "UberEats"
    },
    {
        logo: DoorDash,
        Id: 2,
        income: 581,
        Name: "Door Dash"
    },
    {
        logo: JustEat,
        Id: 3,
        income: 720,
        Name: "Just Eats"
    },
    {
        logo: FoodPanda,
        Id: 4,
        income: 260,
        Name: "FoodPanda"
    }

];

export const Revenue = [
    {
        logo: cup,
        Id: 1,
        income: 510,
        Name: "Net Items"
    },
    {
        logo: Dollar,
        Id: 2,
        income: 321,
        Name: "Net Revenue"
    },
    {
        logo: Orders,
        Id: 3,
        income: 653,
        Name: "Net Orders"
    },
    {
        logo: Customer,
        Id: 4,
        income: 569,
        Name: "Order Holders"
    }

];
export const stock = [
    {
        logo: item,
        Id: 1,
        income: 100,
        Name: "Pizza",
        dailytarget: 800
    },
    {
        logo: item1,
        Id: 2,
        income: 500,
        Name: "Roll Paratha",
        dailytarget: 800
    },
    {
        logo: item2,
        Id: 3,
        income: 400,
        Name: "Brownie",
        dailytarget: 800
    },
    {
        logo: item3,
        Id: 4,
        income: 200,
        Name: "Fries",
        dailytarget: 800
    },
    {
        logo: item5,
        Id: 5,
        income: 569,
        Name: "Pizza",
        dailytarget: 800
    },

];
export const third = [
    {
        logo: Uber,
        Id: 1,
        income: 100,
        Name: "UberEats",
        dailytarget: 800
    },
    {
        logo: FoodPanda,
        Id: 2,
        income: 500,
        Name: "Food Panda",
        dailytarget: 800
    },
    {
        logo: JustEat,
        Id: 3,
        income: 400,
        Name: "Just Eats",
        dailytarget: 800
    },
    {
        logo: item3,
        Id: 4,
        income: 200,
        Name: "Swiggy",
        dailytarget: 800
    },
    {
        logo: DoorDash,
        Id: 5,
        income: 569,
        Name: "DoorDash",
        dailytarget: 800
    },

];
export default thirdParty;