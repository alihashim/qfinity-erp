import CustomPieChart from '../../charts/pieChart';
import arrow from '../../../../assets/images/ArrowRight.png';
import React, { useState } from 'react';
import Select from 'react-select';
import ApiService from '../../../../services/apiService';

const OrderSummery = ({ val }) => {

    const [Summary, setSelectedOption] = useState(null);
    const [orderSumary, SetorderSum] = useState("");
    const SummaryFilter = [
        { value: '0', label: 'Day' },
        { value: '1', label: 'Week' },
        { value: '2', label: 'Month' },
        { value: '3', label: 'Year' },
        { value: '4', label: 'All Time' },
    ];

    const handleTypeSelect = (e) => {
        setSelectedOption(e.value);
        getSumData()
    }

    const getSumData = async () => {
        try {
            const apiService = new ApiService();
            let paramaters = `?GetByFilter=${Summary}`;
            let res = await apiService.get("ORDER_SUMMERY", paramaters);
            SetorderSum(res?.data?.result)
        } catch (error) { }
    }

    return (
        <div className='dashcard'>
            <div className="row ">
                <div className="col-lg-5">
                    <div className="Summary-label">Order Summary</div>
                    <div className="Summary-mini">Lorem ipsum dolor sit amet, consectetur</div>
                </div>
                <div className="col-lg-5">
                    <Select
                        defaultValue={Summary}
                        options={SummaryFilter}
                        onChange={handleTypeSelect}
                        value={SummaryFilter.find(function (option) {
                            return option.value === Summary;
                        })}
                    />
                </div>
            </div>
            <div className='row bg-main-count p-2 mt-4'>
                <div className='col-lg-6 d-flex justify-content-around align-items-center'>
                    <div className='bg-counts'>
                        <div className='count-itself '>
                            25
                        </div>
                    </div>
                    <div className='ord-in-que'>
                        Order in  Process
                    </div>
                </div>
                <div className='col-lg-6 end-center '>
                    <div className='green-mini '>
                        Manage Order
                    </div>
                    <div>
                        <img className='arrow-right' src={arrow} />
                    </div>
                </div>
            </div>
            <div className='row mt-5'>
                <div className='col-lg-4'>
                    <div className='summary-head'> {orderSumary?.onDeliveryCount || "0"}</div>
                    <div className='summary-para'> On Delivery</div>
                </div>
                <div className='col-lg-4'>
                    <div className='summary-head'> {orderSumary?.deliveredCount || "0"}</div>
                    <div className='summary-para'> Delivered</div>
                </div>
                <div className='col-lg-4'>
                    <div className='summary-head'> {orderSumary?.canceledCount || "0"}</div>
                    <div className='summary-para'> Canceled</div>
                </div>
            </div>
            <div className='row'>
                <div className='col-lg-4'>
                    <CustomPieChart val={orderSumary} />
                </div>
                <div className='col-lg-1'></div>
                <div className='col-lg-7 mt-4'>
                    <div className='align-items-center mb-2'>
                        <div>Delivered {orderSumary?.deliveredCount || "0"}%</div>
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div className="ml-2">{orderSumary?.deliveredCount || "0"}</div>
                    </div>
                    <div className='align-items-center mb-2'>
                        <div>On Delivery {orderSumary?.onDeliveryCount || "0"}</div>
                        <div class="progress">
                            <div class="progress-barP bg-orange" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div className="ml-2">{orderSumary?.onDeliveryCount || "0"}</div>
                    </div>
                    <div className='align-items-center mb-2'>
                        <div>Canceled {orderSumary?.canceledCount || "0"} %</div>
                        <div class="progress">
                            <div class="progress-barC bg-dark" role="progressbar" aria-valuenow="7%" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div className="ml-2">{orderSumary?.canceledCount || "0"}</div>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default OrderSummery;
