
import React, { useState } from 'react';
import {
    FaTh,
    FaBars,
    FaUserAlt,
    FaRegChartBar,
    FaCommentAlt,
    FaShoppingBag,
    FaThList,
    FaFirstOrderAlt,
    FaMedapps,
    FaHouseUser
} from "react-icons/fa";
import { NavLink } from 'react-router-dom';
import './sidebars.css'
import logo from "../../assets/images/Logo.png"
const Sidebar = ({ children }) => {
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);
    const menuItem = [
        {
            path:"/Home",
            name:"Dashboard",
            icon:<FaTh/>
        },
        {
            path: "/about",
            name: "About",
            icon: <FaUserAlt />
        },
        {
            path: "/analytics",
            name: "Analytics",
            icon: <FaRegChartBar />
        },
        {
            path: "/orders",
            name: "Orders",
            icon: <FaFirstOrderAlt />
        },
        {
            path: "/menu",
            name: "Menu",
            icon: <FaMedapps />
        },
        {
            path: "/comment",
            name: "Comment",
            icon: <FaCommentAlt />
        },
        {
            path: "/product",
            name: "Product",
            icon: <FaShoppingBag />
        },
        {
            path: "/productList",
            name: "Product List",
            icon: <FaThList />
        },
        {
            path: "/payment",
            name: "Payments",
            icon: <FaCommentAlt />
        },
        {
            path: "/addItems",
            name: "Warehouse",
            icon: <FaHouseUser />
        },
    ]
    return (
        <div className="container-fluid p-0">
            <div className='row'>
                <div style={{ width: isOpen ? "260px" : "60px" }} className="col-lg-3 sidebar pl-2">
                    <div className="top_section">
                        <img style={{ display: isOpen ? "block" : "none" }} src={logo} className="logo" />
                        <div style={{ marginLeft: isOpen ? "50px" : "0px" }} className="bars">
                            <FaBars onClick={toggle} />
                        </div>
                    </div>
                    {
                        menuItem.map((item, index) => (
                            <NavLink to={item.path} key={index} className="link" activeclassName="active">
                                <div className="icon">{item.icon}</div>
                                <div style={{ display: isOpen ? "block" : "none" }} className="link_text">{item.name}</div>
                            </NavLink>
                        ))
                    }
                </div>
                <main className='col-lg-10'>{children}</main>
            </div>
        </div>

    );
};

export default Sidebar;