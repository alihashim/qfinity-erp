import React from "react";
import "./sideBar.css"
import 'react-pro-sidebar/dist/css/styles.css';
import { useParams } from 'react-router-dom';
import Home from "../home";
import Menu from "../menus/menu";
import Order from "../orders/order";
import SideBar from "./sideBar";

const Setting = () => {
    const { id } = useParams()
    return (
        <div >
            <div>
                {(() => {
                    switch (id) {
                        case "Dasboard":
                            return <> <SideBar id={id} /><Home /></>
                            
                        case "Menu":
                            return <><SideBar id={id} /><Menu /></>

                        case "Order":
                            return <><SideBar id={id} /><Order /></>

                        case "Inventory":
                            return <><SideBar id={id} /><Home /></>

                        case "Payment":
                            return <><SideBar id={id} /><Home /></>

                        case "Customers":
                            return <><SideBar id={id} /><Home /></>

                        case "Employees":
                            return <><SideBar id={id} /><Home /></>

                        case "Wastage":
                            return <><SideBar id={id} /><Home /></>

                        case "Maintenance":
                            return <><SideBar id={id} /><Home /></>

                        case "Warehouse":
                            return <><SideBar id={id} /><Home /></>

                        case "Reports&Analytics":
                            return <><SideBar id={id} /><Home /></>

                        default:
                            return <><SideBar id={"Dashboard"} /><Home /></>
                    }
                })()}
            </div>
        </div>
    );
}
export default Setting;