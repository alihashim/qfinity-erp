// PlainLayout.js (without sidebar)
import React from 'react';

const PlainLayout = ({ children }) => {
    return (
        <div className="plain-layout">
            {children}
        </div>
    );
};

export default PlainLayout;
