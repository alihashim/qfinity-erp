import React, { useState } from "react";
import "./sideBar.css";
import Logo from "../../../assets/images/Q Logo.png";
import { NavLink, useLocation } from "react-router-dom";
import { ReactComponent as Dashboard } from '../../../assets/images/svgIcons/Dashboard.svg';
import { ReactComponent as DashboardInactive } from '../../../assets/images/svgIcons/DashboardinActive.svg';
import { ReactComponent as Menu } from '../../../assets/images/svgIcons/menu.svg';
import { ReactComponent as Menuinactive } from '../../../assets/images/svgIcons/menu-inactive.svg';
import { ReactComponent as Order } from '../../../assets/images/svgIcons/order.svg';
import { ReactComponent as Costumer } from '../../../assets/images/svgIcons/costumer.svg';
import { ReactComponent as Employee } from '../../../assets/images/svgIcons/employee.svg';
import { ReactComponent as Reports } from '../../../assets/images/svgIcons/reports.svg';
import { ReactComponent as Inventory } from '../../../assets/images/svgIcons/inventory.svg';
import { ReactComponent as Maintenance } from '../../../assets/images/svgIcons/Maintenance.svg';
import { ReactComponent as Payment } from '../../../assets/images/svgIcons/payment.svg';
import { ReactComponent as Wastage } from '../../../assets/images/svgIcons/wastage.svg';
import { ReactComponent as WearHouse } from '../../../assets/images/svgIcons/wearHouse.svg';

const SideBar = () => {

    const location = useLocation();
    return (
        <nav class="navbar navbar-expand-md  fixed-left ">
            <a href="/" class="navbar-brand center" className="logo"><img src={Logo} alt="Logo" clas/></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <NavLink to="/Home" className="nav-link align-middle" activeClassName="active">
                            {location.pathname === "/Home" ? (
                                <>
                                    <Dashboard size={25} className="" />
                                    <a className="nav-link align-middle">
                                        <h6 className="d-none d-sm-inline  activetxt" >Home</h6>
                                    </a>

                                </>
                            ) : (
                                <>
                                    <DashboardInactive size={25} className="" />
                                    <a className="nav-link align-middle">
                                        <h6 className="d-none d-sm-inline  inactivetxt">Home</h6>
                                    </a>
                                </>
                            )}
                        </NavLink>
                    </li>
                    <li class="nav-item">
                        <NavLink to="/menu" className="nav-link align-middle" activeClassName="active">
                            {location.pathname === "/menu" ? (
                                <>
                                    <Menuinactive size={25} className="" />
                                    <a className="nav-link align-middle">
                                        <h6 className="d-none d-sm-inline  activetxt">Menus</h6>
                                    </a>
                                </>
                            ) : (
                                <>
                                    <Menu size={25} className="" />
                                    <a className="nav-link align-middle">
                                        <h6 className="d-none d-sm-inline  inactivetxt">Menus</h6>
                                    </a>
                                </>
                            )}
                        </NavLink>
                    </li>
                    <li class="nav-item">
                        <NavLink to="/orders" className="nav-link align-middle" activeClassName="active">

                            {location.pathname === "/orders" ? (
                                <>
                                    <Menuinactive size={25} className="" />
                                    <a className="nav-link align-middle">
                                        <h6 className="d-none d-sm-inline  activetxt">Orders</h6>
                                    </a>
                                </>
                            ) : (
                                <>
                                    <Order size={25} className="" />
                                    <a className="nav-link align-middle">
                                        <h6 className="d-none d-sm-inline  inactivetxt">Orders</h6>
                                    </a>
                                </>
                            )}
                        </NavLink>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <NavLink to="/payment" className="nav-link align-middle" activeClassName="active">
                            {location.pathname === "/payment" ? (
                                <>
                                    <Menuinactive size={25} className="" />
                                    <a className="nav-link align-middle">
                                        <h6 className="d-none d-sm-inline  activetxt">Payment</h6>
                                    </a>
                                </>
                            ) : (
                                <>
                                    <Payment size={25} className="" />
                                    <a className="nav-link align-middle">
                                        <h6 className="d-none d-sm-inline  inactivetxt">Payment</h6>
                                    </a>
                                </>
                            )}
                        </NavLink>
                    </li>
                    <li class="nav-item">
                        <NavLink to="/costumer/menu" className="nav-link align-middle" activeClassName="active">
                            {location.pathname === "//costumer/menu" ? (
                                <>
                                    <Menuinactive size={25} className="" />
                                    <a className="nav-link align-middle">
                                        <h6 className="d-none d-sm-inline  activetxt">Customer</h6>
                                    </a>
                                </>
                            ) : (
                                <>
                                    <Costumer size={25} className="" />
                                    <a className="nav-link align-middle">
                                        <h6 className="d-none d-sm-inline  inactivetxt">Customer</h6>
                                    </a>
                                </>
                            )}
                        </NavLink>
                    </li>
                    <li class="nav-item">
                        <NavLink to="/Employees" className="nav-link align-middle" activeClassName="active">
                            <Employee size={25} className="" />
                            <a className="nav-link align-middle">
                                <h6 className="d-none d-sm-inline inactivetxt">Employees</h6>
                            </a>
                        </NavLink>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <NavLink to="/costumer/wastage" className="nav-link align-middle" activeClassName="active">
                            <Wastage size={25} className="" />
                            <a className="nav-link align-middle">
                                <h6 className="d-none d-sm-inline inactivetxt">Wastage</h6>
                            </a>
                        </NavLink>
                    </li>
                    <li class="nav-item">
                        <NavLink to="/admin/Maintenance" className="nav-link align-middle" activeClassName="active">
                            <Maintenance size={25} className="" />
                            <a className="nav-link align-middle">
                                <h6 className="d-none d-sm-inline inactivetxt">Maintenance</h6>
                            </a>
                        </NavLink>
                    </li>
                    <li class="nav-item">
                        <NavLink to="/costumer/Warehouse" className="nav-link align-middle" activeClassName="active">
                            <WearHouse size={25} className="" />
                            <a className="nav-link align-middle">
                                <h6 className="d-none d-sm-inline inactivetxt">Wearhouse</h6>
                            </a>
                        </NavLink>
                    </li>
                    <li class="nav-item">
                        <NavLink to="/cashier" className="nav-link align-middle" activeClassName="active">
                            <Reports size={25} className="" />
                            <a className="nav-link align-middle">
                                <h6 className="d-none d-sm-inline inactivetxt">Reports & Analytics</h6>
                            </a>
                        </NavLink>
                    </li>
                    <li class="nav-item">
                        <NavLink to="/Inventory" className="nav-link align-middle" activeClassName="active">
                            <Inventory size={25} className="" />
                            <a className="nav-link align-middle">
                                <h6 className="d-none d-sm-inline inactivetxt">Inventory</h6>
                            </a>
                        </NavLink>
                    </li>
                </ul>
            </div>
        </nav>

    );

}
export default SideBar