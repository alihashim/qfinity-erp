// SidebarLayout.js
import React from 'react';
import { useLocation } from 'react-router-dom';
import SideBar from './sideBar';
import "./sideBar.css"

const SidebarLayout = ({ children }) => {
    const location = useLocation();

    const isSidebarEnabled = !location.pathname.startsWith('/home'); // Decide whether to show the sidebar

    return (
        <div className="sidebar-layout">
            <div className={`sidebar ${isSidebarEnabled ? 'fixed-left' : ''}`}>
                {isSidebarEnabled && <SideBar currentPath={location.pathname} />}
            </div>
            <main className="content">{children}</main>
        </div>
    );
};

export default SidebarLayout;
