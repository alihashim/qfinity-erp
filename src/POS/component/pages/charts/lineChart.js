import React from 'react'
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

const lineChart = () => {
    const data = [
        { name: 2002, UberEats: 200, DoorDash: 350, foodPanda: 280 },
        { name: 2003, UberEats: 400, DoorDash: 210, foodPanda: 320 },
        { name: 2004, UberEats: 300, DoorDash: 460, foodPanda: 190 },
        { name: 2005, UberEats: 600, DoorDash: 370, foodPanda: 420 },
        // Add more data points for your best-selling items
    ];
    return (
        <>
            <div className='container-fluid'>
                <LineChart width={750} height={320} data={data}>
                    {/* 
                <YAxis />
                <Tooltip />
                <Legend />*/}
                    {/* <XAxis dataKey="name" /> */}

                    <Line type="monotone" dataKey="UberEats" stroke="green" strokeWidth={2} />
                    <Line type="monotone" dataKey="DoorDash" stroke="red" strokeWidth={2} />
                    <Line type="monotone" dataKey="foodPanda" stroke="pink" strokeWidth={2} />
                    <CartesianGrid strokeDasharray="3 3" />
                    {/* <XAxis dataKey="name" /> 
                <YAxis dataKey="name" />  */}
                    <Tooltip />
                    <Legend />
                </LineChart>
            </div>
        </>

    )
}
export default lineChart