import React from 'react';
import { PieChart, Pie, Sector, Cell, ResponsiveContainer } from 'recharts';

// import { PieChart, Pie, Cell, ResponsiveContainer } from 'recharts';

// const data = [
//   { name: 'Delivered', value: 30 },
//   { name: 'On Delivery', value: 20 },
//   { name: 'Canceled', value: 10 },
// ];

const COLORS = ['#2BC155', '#FF6D4C', '#000000'];


const CustomPieChart = ({ val }) => {

  let onDeliveryPerc = ((val?.onDeliveryCount / val?.totalOrders) * 100).toFixed(2);
  let DeliveredPerc = ((val?.deliveredCount / val?.totalOrders) * 100).toFixed(2);
  let canceledPerc = ((val?.canceledCount / val?.totalOrders) * 100).toFixed(2);
  
  const dynamicData = [
    { name: 'On Delivery', value: parseFloat(onDeliveryPerc) },
    { name: 'Canceled', value: parseFloat(canceledPerc) },
    { name: 'Delivered', value: parseFloat(DeliveredPerc) },
  ];
  return (
    <ResponsiveContainer >
      <PieChart width={100} height={100}>
        <Pie
          data={dynamicData}
          dataKey="value"
          nameKey="name"
          cx="50%" cy="50%"
          innerRadius={60}
          outerRadius={80}
          paddingAngle={3}
          fill="#8884d8"
          label
        />
        {dynamicData.map((entry, index) => (
          <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
        ))}
        {/* <Pie
          data={dynamicData}
          cx={100}
          cy={120}
          innerRadius={60}
          outerRadius={80}
          fill="#8884d8"
          paddingAngle={5}
          dataKey="value"
          viewBox='50 25 100 200'
          startAngle={90}
        // label={true}
        >
          {dynamicData.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
        </Pie> */}
      </PieChart>
    </ResponsiveContainer>
  )
}
export default CustomPieChart;
