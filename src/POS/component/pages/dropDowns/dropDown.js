import React, { useState } from 'react';
import "./dropDown.css";

function Dropdown() {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);
  const options = [
    "Option 1",
    "Option 2",
    "Option 3",
    // Add more options as needed
  ];

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const selectOption = (option) => {
    setSelectedOption(option);
    setIsOpen(false);
  };

  return (
    <div className="dropdown-container">
      <div className={`dropdown-header w-100 ${isOpen ? 'open' : ''}`} onClick={toggleDropdown}>
        <span className="header-text">
          {selectedOption || "Select an option"}
        </span>
        {/* <span className={` arrow-icon ${isOpen ? 'up' : 'down'}`}>&#9660;</span> */}
      </div>
      {isOpen && (
        <ul className="dropdown-list">
          {options.map((option) => (
            <li key={option} onClick={() => selectOption(option)}>
              {option}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}

export default Dropdown;
