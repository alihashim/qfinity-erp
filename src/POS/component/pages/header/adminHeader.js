import { React, useState, useEffect } from 'react'
import Profile from "../../../assets/images/profileImg.png"
import "./header.css"
import { useLocation } from 'react-router-dom';

const AdminHeader = () => {
    const [headerText, setHeaderText] = useState('Dashboard');
    const location = useLocation();

    useEffect(() => {
        const { pathname } = location;
        if (pathname === '/menu') {
            setHeaderText('Menu');
        } else if (pathname === '/orders') {
            setHeaderText('Orders');
        } else if (pathname.includes('/order/')) {
            setHeaderText('Orders');
        } else if (pathname === '/Home') {
            setHeaderText('Dashboard');
        } else if (pathname === '/ordersDND') {
            setHeaderText('Order Drag and Drop');
        } else if (pathname === '/additems') {
            setHeaderText('Add Item');
        } else if (pathname === '/singleMenu') {
            setHeaderText('Single Item');
        } else {
            setHeaderText('Dashboard'); // Default header text
        }
    }, [location]);
    return (
        <nav class="navbar-expand-lg ">
            <div class="container-fluid px-lg-5">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarSupportedContent">
                    <div>
                        <div className='Headtxt'>{headerText}</div>
                        <div className='headtxt2'>Welcome to Qinfinity Admin!</div>
                    </div>
                    <div className='d-flex justify-content-end end'>
                        <div className='profilebox'>
                            Asad,Khan
                        </div>
                        <div className='adminprofileimgbox '>
                            <img src={Profile} alt='profile' />
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default AdminHeader