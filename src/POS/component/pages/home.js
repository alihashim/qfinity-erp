import React from 'react';
import './homes.css'; // Import your styles
import arrow from '../../assets/images/ArrowRight.png';
import Cup from '../../assets/images/Cup.png';
import foodpanda from "../../assets/images/Logos/foodpanda.png"
import justeat from "../../assets/images/Logos/justeats.png"
import ubereats from "../../assets/images/Logos/pngegg.png"
import doordash from "../../assets/images/Logos/doordash.png"
import Customers from '../../assets/images/Customer.png';
import Revenue from '../../assets/images/Revenue.png';
import graph from '../../assets/images/Graph.png';
import Order from '../../assets/images/Orders.png';
import LineChart from './charts/lineChart';
import CustomPieChart from './charts/pieChart';
function Home() {
  return (
    <div>
      {/* <AdminHeader /> */}
      <div className="container-fluid m-0 p-0 p-5">
        <div className="row">
          <div className='col-lg-10'>
            <div className="Heads pt-3 pl-3">Dashboard</div>
            <div className="SilverHeads">Welcome to Qinfinity Admin!</div>
          </div>
          <div className='col-lg-2 d-flex'>
            <ul class="nav nav-pills d-flex justify-content-between center" id="pills-tab" role="tablist">
              <li class="nav-item" role="presentation">
                <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Monthly</button>
              </li>
              <li class="nav-item" role="presentation">
                <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Weekly</button>
              </li>
              <li class="nav-item" role="presentation">
                <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                  data-bs-target="#pills-contact" type="button" role="tab"
                  aria-controls="pills-contact" aria-selected="false">Today
                </button>
              </li>
            </ul>
          </div>
        </div>
        <div className="row justify-content-between mt-5">
          {renderCard(Cup, '124', 'Net Item')}
          {renderCard(Revenue, '12K', 'Net Revenue')}
          {renderCard(Order, '279', 'Net Orders')}
          {renderCard(Customers, '12K', 'Net Customer')}
        </div>
        <div className="row justify-content-between">
          {renderCard(ubereats, '351', 'Uber Eats')}
          {renderCard(doordash, '165', 'Door Dash')}
          {renderCard(justeat, '479', 'Just Eats')}
          {renderCard(foodpanda, '515', 'Food Panda')}
        </div>
        <div className="row">
          <div className="col-lg-6">
            <div className="miniLabels">Sales by Delivery Sources</div>
            <div className="card-text">$ 5.987,37</div>
            <LineChart />
          </div>
          <div className="col-lg-6">
            <div className="row">
              <div className="col-lg-5">
                <div className="Summary-label">Order Summary</div>
                <div className="Summary-mini">Lorem ipsum dolor sit amet, consectetur</div>
              </div>
              <div className="col-lg-2"></div>
              <div className="col-lg-5">
                <div className="d-flex justify-content-between">
                  <div>Monthly</div>
                  <div>Weekly</div>
                  <div>Today</div>
                </div>
              </div>
            </div>
            <div className='row bg-main-count p-2 mt-4'>
              <div className='col-lg-6 d-flex justify-content-around align-items-center'>
                <div className='bg-counts'>
                  <div className='count-itself '>
                    25
                  </div>
                </div>
                <div className='ord-in-que'>
                  Order in  queue
                </div>
              </div>
              <div className='col-lg-6 end-center '>
                <div className='green-mini '>
                  Manage Order
                </div>
                <div>
                  <img className='arrow-right' src={arrow} />
                </div>
              </div>
            </div>
            <div className='row mt-5'>
              <div className='col-lg-4'>
                <div className='summary-head'> 26</div>
                <div className='summary-para'> On Delivery</div>
              </div>
              <div className='col-lg-4'>
                <div className='summary-head'> 14</div>
                <div className='summary-para'> Delivered</div>
              </div>
              <div className='col-lg-4'>
                <div className='summary-head'> 6</div>
                <div className='summary-para'> Canceled</div>
              </div>
            </div>
            <div className='row'>
              <div className='col-lg-4'>
                <CustomPieChart />
              </div>
              <div className='col-lg-1'></div>
              <div className='col-lg-7 mt-4'>
                <div className='align-items-center mb-2'>
                  <div>Delivered (25%)</div>
                  <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <div className="ml-2">25</div>
                </div>
                <div className='align-items-center mb-2'>
                  <div>On Delivery (12%)</div>
                  <div class="progress">
                    <div class="progress-barP bg-orange" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <div className="ml-2">15</div>
                </div>
                <div className='align-items-center mb-2'>
                  <div>Canceled (7%)</div>
                  <div class="progress">
                    <div class="progress-barC bg-dark" role="progressbar" aria-valuenow="7%" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <div className="ml-2">7</div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div className='row'>
          <div className='col-lg-12'>
            {/* <CustomBarChart /> */}
            <img className='w-100' src={graph} />
          </div>
        </div>
      </div>
    </div>
  );
}

function renderCard(image, text, title) {
  return (
    <div className="col-lg-3 p-5">
      <div className="card card-radious">
        <div className="p-4 row center">
          <div className="col-lg-5 center d-flex justify-content-center">
            <img className="DashIcon" src={image} alt="Cup" />
          </div>
          <div className="col-lg-7">
            <p className="card-text center m-0">{text}</p>
            <p className="card-title center m-0">{title}</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
