import React from 'react'
import ChefIcon from "../../../assets/images/chefIcon.png";
import { useNavigate } from 'react-router-dom';

function AddItembox() {
    let navigate = useNavigate();
  const handleAddItem = () => {
    navigate('/additems');
  }
    return (
        <div className='linear-box w-40 justify-content-center p-2'>
            <div className='absolute'>
                <img src={ChefIcon} />
            </div>
            <div className='row pt-lg-4 mt-lg-3 pt-md-3'>
                <div className=''>
                    Organize your menus through button bellow
                </div>
            </div>
            <div className='pl-3 pr-3 pb-3 pt-1 justify-content-center row'>
                <div className=' row  w-75 addItems-btn p-2' onClick={handleAddItem}>
                    + Add items
                </div>
            </div>

        </div>)
}

export default AddItembox