import React, { useState, useEffect } from 'react';
import './addmenu.css';
import ImageUpload from "./imagUpload";
import CsvFileReader from "./uploadCSV/uploadCSv";
import CreatableSelect from 'react-select/creatable';
import ApiService from '../../../services/apiService';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate, useLocation } from 'react-router-dom';

const AddItem = (props) => {
  let navigate = useNavigate();
  const location = useLocation();

  const [formData, setFormData] = useState({
    name: '',
    discount: '',
    description: '',
    price: '',
    category: '',
    delivery: '',
    addons: [{ name: '', price: '' }],
    image: null,
  });

  const [categories, setCategories] = useState([]);

  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const apiService = new ApiService()
        const response = await apiService.get('GET_CATEGORY');
        const updatedCategories = response?.data?.result;
        setCategories(updatedCategories);
      } catch (error) {
        console.error('Error fetching categories:', error);
      }
    };

    fetchCategories();
    if (location.state && location.state.item) {
      const { item } = location.state;
      setFormData({
        name: item.name || '',
        discount: item.discount || '',
        description: item.description || '',
        price: item.price || '',
        category: item.category || '',
        delivery: item.delivery || '',
        addons: item.addons || [{ name: '', price: '' }],
      });
    }
  }, [location.state]);

  const handleCategoryChange = (selectedOption) => {
    setFormData({ ...formData, category: selectedOption?.label || selectedOption });
  };

  const handleCategoryCreate = async (inputValue) => {
    try {

      let body = {
        id: 0,
        name: inputValue
      }

      const apiService = new ApiService()

      let Create_Respose = await apiService.post('CREATE_CATEGORY', body);
      

    } catch (error) {
      console.error('Error adding category:', error);
    }
  };
  const handleInputChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleAddonChange = (index, e) => {
    const newAddons = [...formData.addons];
    newAddons[index][e.target.name] = e.target.value;
    setFormData({ ...formData, addons: newAddons });
  };

  const handleAddAddon = () => {
    setFormData({ ...formData, addons: [...formData.addons, { name: '', price: '' }] });
  };

  const handleRemoveAddon = (index) => {
    const newAddons = [...formData.addons];
    newAddons.splice(index, 1);
    setFormData({ ...formData, addons: newAddons });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const postData = {
        id: 0,
        name: formData.name,
        description: formData.description,
        price: formData.price,
        imageUrl: "sdfs",
        category: {
          id: formData.category[0]?.value?.id,
          name: formData.category[0]?.value?.name,
        },
        addOns: formData.addons.map(addon => ({
          id: 0,
          name: addon.name,
          price: addon.price,
        })),
      };

      const apiService = new ApiService();
      const endpoint = location.state && location.state.item ? 'UPDATE_MENU' : 'CREATE_MENU';

      const response = await apiService.post(endpoint, postData);

      if (response.data.statusCode === 201) {
        toast.success('Menu item created/updated successfully');
        navigate('/menu');
      } else {
        toast.warning('Waiting for menu update');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };


  return (
    <div className='container-fluid padds pad-top'>
      <h2>Add Item</h2>
      <div>
        <ImageUpload />
      </div>
      <div className="pb-lg-5">
        <CsvFileReader />
      </div>
      <form onSubmit={handleSubmit}>
        <div className=''>
          <div className="form-group row justify-content-between">
            <div className="col-md-5 col-lg-5">
              <label htmlFor="validationCustom01" className="form-label">Name</label>
              <input
                type="text"
                className="p-2 form-control"
                id="validationCustom01"
                name="name"
                value={formData.name}
                onChange={handleInputChange}
                required
              />
              <div className="valid-feedback">
                Looks good!
              </div>
            </div>

            <div className="col-md-6">
              <label htmlFor="validationCustom02" className="form-label">Description</label>
              <input
                type="text"
                className="p-2 form-control"
                id="validationCustom02"
                name="description"
                value={formData.description}
                onChange={handleInputChange}
                required
              />
              <div className="valid-feedback">
                Looks good!
              </div>
            </div>
          </div>

          <div className="form-group row mt-3 justify-content-between">
            <div className="col-md-5 col-lg-5">
              <label htmlFor="validationCustom03" className="form-label">Price</label>
              <input
                type="text"
                className="p-2 form-control"
                id="validationCustom03"
                name="price"
                value={formData.price}
                onChange={handleInputChange}
                required
              />
              <div className="valid-feedback">
                Looks good!
              </div>
            </div>

            <div className="col-md-6">
              <label htmlFor="validationCustom04" className="form-label">Discount</label>
              <input
                type="text"
                className="p-2 form-control"
                id="validationCustom04"
                name="discount"
                value={formData.discount}
                onChange={handleInputChange}
                required
              />
              <div className="valid-feedback">
                Looks good!
              </div>
            </div>
          </div>

          <div className="form-group row mt-3 justify-content-between">
            <div className="col-md-5 col-lg-5">
              <label htmlFor="validationCustom05" className="form-label">
                Category
              </label>
              <CreatableSelect
                isMulti
                options={categories.map((category) => ({ value: category, label: category.name }))}
                value={formData.category}
                onChange={handleCategoryChange}
                onCreateOption={handleCategoryCreate}
                placeholder="Select or create a category"
                className='p-4'
                required
              />
              <div className="valid-feedback">Looks good!</div>
            </div>

            <div className="col-md-6">
              <label htmlFor="validationCustom06" className="form-label">Delivery</label>
              <input
                type="text"
                className="p-2 form-control"
                id="validationCustom06"
                name="delivery"
                value={formData.delivery}
                onChange={handleInputChange}
                required
              />
              <div className="valid-feedback">
                Looks good!
              </div>
            </div>
          </div>

          {/* Add more fields as needed */}

        </div>

        <div className="row mt-5">
          <div className="addItem-head">Modifiers</div>
          <div className='col-lg-2 col-md-2 addMore-btn p-2 mt-4 mb-3' onClick={handleAddAddon}>
            Add more
          </div>
          {formData.addons.map((addon, index) => (
            <div key={index} className="form-group row mt-2 justify-content-between align-content-center">
              <div className="col-md-5">
                <label htmlFor={`addonName${index}`} className="form-label">Name</label>
                <input
                  type="text"
                  className="p-2 form-control"
                  id={`addonName${index}`}
                  name="name"
                  value={addon.name}
                  onChange={(e) => handleAddonChange(index, e)}
                  required
                />
              </div>
              <div className="col-md-5">
                <label htmlFor={`addonPrice${index}`} className="form-label">Price</label>
                <input
                  type="text"
                  className="p-2 form-control"
                  id={`addonPrice${index}`}
                  name="price"
                  value={addon.price}
                  onChange={(e) => handleAddonChange(index, e)}
                  required
                />
              </div>
              <div className="col-md-2 center pt-lg-4">
                <button
                  type="button"
                  className="btn btn-danger"
                  onClick={() => handleRemoveAddon(index)}
                >
                  Remove
                </button>
              </div>
            </div>
          ))}

        </div>
        <div className='row mt-5'>
          <div className='end'>
            <button className='addItem-btn' type="submit">Add Item</button>
          </div>
        </div>
      </form>
    </div>
  );
}

export default AddItem;
