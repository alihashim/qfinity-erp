import React, { useState, useRef } from 'react';
import UploadIcon from "../../../assets/images/svgIcons/UploadIcon.svg";
import './imageUpload.css';

const ImageUpload = () => {
  const [images, setImages] = useState([]);
  const [isDragging, setIsDragging] = useState(false);
  const fileInputRef = useRef(null);

  const selectFiles = () => {
    fileInputRef.current.click();
  };

  const deleteImage = (index) => {
    setImages((previousImages) => previousImages.filter((_, i) => i !== index));
  };
  const onDragLeave = (event) => {
    event.preventDefault()
    setIsDragging(true)
    event.dataTransfer.dropEffect = "copy";
  };
  const onDragOver = (event) => {
    event.preventDefault()
    setIsDragging(true)
  };
  const onDrop = (event) => {
    event.preventDefault()
    setIsDragging(true)
    let files = event.dataTransfer.files
    for (let i = 0; i < files.length; i++) {
      if (files[i].type.split('/')[0] !== 'image') continue;

      if (!images.some((e) => e.name === files[i].name)) {
        setImages((prevImages) => [
          ...prevImages,
          {
            name: files[i].name,
            url: URL.createObjectURL(files[i]),
          },
        ]);
      }
    }
  };
  const onFileSelect = (event) => {
    const files = event.target.files;

    if (files.length === 0) {
      return;
    }

    for (let i = 0; i < files.length; i++) {
      if (files[i].type.split('/')[0] !== 'image') continue;

      if (!images.some((e) => e.name === files[i].name)) {
        setImages((prevImages) => [
          ...prevImages,
          {
            name: files[i].name,
            url: URL.createObjectURL(files[i]),
          },
        ]);
      }
      
    }
  };

  return (
    <div className="container-fluid uploadBox mb-5">
      <div className=''>
        <div className="drag-area p-5 justify-content-center" onClick={selectFiles} onDragOver={onDragOver} onDragLeave={onDragLeave} onDrop={onDrop}>
          <div className=' justify-content-center d-flex'>
            <img onClick={selectFiles} src={UploadIcon} alt="Upload" />
          </div>
          {isDragging ? (
            <div onClick={selectFiles} className="select justify-content-center d-flex">Upload OR Drop images here </div>
          ) : (
            <span className='justify-content-center d-flex'>
              Drag & Drop images here OR Browse 
            </span>
          )}
          <input
            name="file"
            type="file"
            className="file"
            multiple
            ref={fileInputRef}
            onChange={onFileSelect}
          />
        </div>
        <div className="container">
          {images.map((image, index) => (
            <div className="image" key={index}>
              <span className="delete" onClick={() => deleteImage(index)}>&times;</span>
              <img src={image.url} alt={image.name} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ImageUpload;
