import React, { useState, useRef, useEffect } from 'react';
import "../homes.css";
import dahboard from "../../../assets/images/darhboard.png";
import MenuItemCard from './menuItemCard';
import "./menuItemcard.css"
import MenuItemTable from "./menuItemTable";
import AddItembox from './addItembox';
import ApiService from '../../../services/apiService';
import CategoryNavBar from './categoryNav/categoryNav';
import { useNavigate } from 'react-router-dom';
const Menu = () => {
    const [selectedCategory, setSelectedCategory] = useState('All');
    const categoryContainers = useRef([]);
    const [menu, setmenu] = useState([]);
    const navigate = useNavigate()
    useEffect(() => {
        GetMenu();
    }, []);
    const handleEdit =(item)=>{
        navigate('/addItems', { state: { item } });
    }
    const handleDelete =()=>{

    }

    const handleCategorySelect = (category) => {
        setSelectedCategory(category);

        if (category !== 'All') {
            const categoryIndex = categoryContainers.current.findIndex(
                (container) => container.id === category
            );

            if (categoryIndex !== -1) {
                categoryContainers.current[categoryIndex].scrollIntoView({
                    behavior: 'smooth',
                });
            }
        } else {
            window.scrollTo({ top: 0, behavior: 'smooth' });
        }
    };

    const [view, setView] = useState('card');

    const toggleView = () => {
        setView(view === 'card' ? 'table' : 'card');
    };

    const GetMenu = async () => {
        const apiService = new ApiService();
        let res = await apiService.get("GET_MENU");
        setmenu(res.data.result)
    }

    return (
        <div className='container-fluid p-lg-5 m-0  pad-topp'>
            <div className='row'>
                <div className='col-lg-6 col-md-6 mbl-center'>
                    <div className="Heads pt-3 pl-3">Item Menu</div>
                    <div className="SilverHeads">Here is your item list</div>

                </div>
                <div className='col-lg-6 col-md-6'>
                    <div className='d-flex justify-content-end pt-top mbl-center'>
                        <AddItembox />
                    </div>
                </div>
            </div>
            <div className='d-flex gap-4 pt-3'>
                <input type="search" class="form-control w-25" placeholder="Search here" aria-label="Search" aria-describedby="search-addon" />
                <img onClick={toggleView} src={dahboard} className='filter' />
            </div>
            <div>
                <div className="menu mt-5 ">
                    <div className="pt-5">
                        <CategoryNavBar />
                    </div>

                    <div className="menu-items pt-5">
                        {view === 'table' &&
                            <div className='table-responsive'>
                                <table className="table" >
                                    <thead>
                                        <tr>
                                            <th>Items</th>
                                            <th>Qty</th>
                                            <th>Price</th>
                                            <th>Discount %</th>
                                            <th>Total Price</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {menu.map((item, itemIndex) => (
                                            <MenuItemTable key={itemIndex} {...item} />
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        }
                        <div className="row">
                            {
                                view === 'card' && menu.map((item, itemIndex) => (
                                    <MenuItemCard
                                        key={itemIndex}
                                        {...item}
                                        onEdit={() => handleEdit(item)}
                                        onDelete={() => handleDelete(item)}
                                    />
                                ))
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Menu;
