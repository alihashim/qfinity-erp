import React from 'react';
import './menuItemcard.css'
import 'bootstrap'
import P1 from '../../../assets/images/P1.png';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { useState } from 'react';
import ApiService from '../../../services/apiService';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom'

const MenuItemCard = ({ itemPic = P1,
    price,
    id,
    name,
    description, onEdit, onDelete, freedelivery, }) => {


    const [show, setShow] = useState(false);
    let navigate = useNavigate();

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const DeleteItem = async () => {
        const apiService = new ApiService();
        let paramaters = `${id}`;
        let res = await apiService.delete("DELETE_ITEM", paramaters);
        if (res?.data?.statusCode === 204) {

            // Show success toast
            setShow(false)
            window. location. reload(false);
            toast.success("MenuItem Successfully Deleted");
        }
        else {
            toast.warning('Waiting for menu update');
        }
    }
    return (
        <div className='col-lg-3 col-md-6 col-sm-6'>
            <div className="menu-item-card mt-5">
                <div className="menu-item-header p-0">
                    {id && (
                        <div className="special-discount-badge">
                            {id}% Off
                        </div>
                    )}
                    {freedelivery && (
                        <div className="special-discount-badge min-top">
                            Free delivery
                        </div>
                    )}
                </div>
                <div className="menu-item-details justify-content-center IMG-BG">
                    <div><img src={itemPic} className='item-pic' /></div>
                </div>
                <div className="menu-item-details justify-content-center p-3">
                    <div className="item-name">{name}</div>
                    <div className="item-des mt-1">{description}</div>
                    <div className="item-price mt-1">${price}</div>
                    <div className="actions row p-1 mt-3">
                        <div className='col-6'>
                            <button className="edit-button" onClick={onEdit}>
                                Edit
                            </button>
                        </div>
                        <div className='col-6'>
                            <Button variant="primary" onClick={handleShow} className='delete-button'>
                                Delete
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Delete Item</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you Sure you want to delete {name}</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button variant="primary" onClick={() => DeleteItem()}>
                        Delete
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
};

export default MenuItemCard;
