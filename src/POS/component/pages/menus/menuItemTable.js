import React from 'react';
import PropTypes from 'prop-types';
import P1 from '../../../assets/images/P1.png';
import DeleteModal from './deleteModal';

const MenuItemCard = ({
    itemPic = P1,
    price,
    id,
    name,
    itemCategory,
    reviews,
}) => (
    <tr>
        <td className='d-inline-flex'>
            <img src={itemPic} alt={name} className="menu-card-image w-50" />
            <div className="menu-card-details">
                <h5>{name}</h5>
                <div className="menu-card-rating">
                    {/* Display stars and reviews */}
                    <span>&#9733;&#9733;&#9733;&#9733;&#9733;</span>
                    <span>{reviews} Reviews</span>
                </div>
                <p>{itemCategory}</p>
            </div>
        </td>
        <td>Qty</td>
        <td>${price}</td>
        <td>{id}%</td>
        <td>$ { price - id}</td>
        <td>
            <div className=' d-flex gap-3'>
                <button className='edit-button'>
                    Edit
                </button>
                <button className='delete-button'>
                    Delete
                </button>
            </div>
        </td>
    </tr>
);

MenuItemCard.propTypes = {
    itemPic: PropTypes.string,
    itemPrice: PropTypes.number.isRequired,
    specialDiscount: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    itemCategory: PropTypes.string.isRequired,
    reviews: PropTypes.number,
};

export default MenuItemCard;
