import React,{useState} from 'react'
import P1 from '../../../assets/images/P1.png'
import "./singleItem.css"
const SingleItem = () => {
    const [activeButton, setActiveButton] = useState('small'); // Set the initial active button

    const handleButtonClick = (button) => {
        setActiveButton(button);
    }
    const [count, setCount] = useState(0);

    const handleIncrement = () => {
        setCount(count + 1);
    }

    const handleDecrement = () => {
        if (count > 0) {
            setCount(count - 1);
        }
    }
    return (
        <div className='container-fluid p-5'>
            <div className='row'>
                <div className='col-lg-5 col-md-5 img-border p-0'>
                    <img className='w-100 d-flex' src={P1} />
                </div>
                <div className='col-lg-6 col-lg-6 padds'>
                    <div className='row'>
                        <div className='SubHead mb-3'>
                            <span className='fw-bold'>Availability : </span>
                            <span>
                                Only 2 in Stock
                            </span>
                        </div>
                        <div className='row'>
                            <div className='Head'>
                                Chicken Pepperoni Pizza
                            </div>
                        </div>
                        <div className='row'>
                            <div class="rating">

                                <span class="star">&#9733;</span>
                                <span class="star">&#9733;</span>
                                <span class="star">&#9733;</span>
                                <span class="star">&#9733;</span>
                            </div>
                        </div>
                        <div>

                            <ul >
                                <li className='list'>α9 Gen5 AI Processor with AI Picture Pro & AI 4K Upscaling</li>
                                <li className='list'>Pixel Dimming, Perfect Black, 100% Color Fidelity & Color Volume</li>
                                <li className='list'>Hands-free Voice Control, Always Ready</li>
                                <li className='list'>Dolby Vision IQ with Precision Detail, Dolby Atmos, Filmmaker Mode</li>
                                <li className='list'>Eye Comfort Display: Low-Blue Light, Flicker-Free</li>
                            </ul>
                        </div>
                        <div className="row">
                            <div className='row mt-2'>
                                <div className='col-lg-3 col-md-3'>
                                    <button
                                        className={`btn w-100 btn-sm  ${activeButton === 'small' ? 'size-btn' : 'unActive'}`}
                                        onClick={() => handleButtonClick('small')}
                                    >
                                        Small
                                    </button>
                                </div>
                                <div className='col-lg-3 col-md-3'>
                                    <button
                                        className={ `w-100 btn btn-sm ${activeButton === 'medium' ? 'size-btn' : 'unActive'}`}
                                        onClick={() => handleButtonClick('medium')}
                                    >
                                        Medium
                                    </button>
                                </div>
                                <div className='col-lg-3 col-md-3'>
                                    <button
                                        className={`w-100 btn btn-sm  ${activeButton === 'large' ? 'size-btn' : 'unActive'}`}
                                        onClick={() => handleButtonClick('large')}
                                    >
                                        Large
                                    </button>
                                </div>
                            </div>
                            <div className='row mt-4'>
                                <div className='col-lg-3 col-md-3'>
                                    <button
                                        className={`w-100 btn btn-sm  ${activeButton === 'xLarge' ? 'size-btn' : 'unActive'}`}
                                        onClick={() => handleButtonClick('xLarge')}
                                    >
                                        X-large
                                    </button>
                                </div>
                                <div className='col-lg-3 col-md-3'>
                                    <button
                                        className={`w-100 btn btn-sm  ${activeButton === 'cumbo' ? 'size-btn' : 'unActive'}`}
                                        onClick={() => handleButtonClick('cumbo')}
                                    >
                                        Cumbo
                                    </button>
                                </div>
                            </div>
                            <div className='mb-4 mt-4'>
                                    <div className='subHead2'>
                                    USD(incl. of all taxes)
                                    </div>
                                    <div className='Head2'>
                                    $600.72
                                    </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    )
}

export default SingleItem