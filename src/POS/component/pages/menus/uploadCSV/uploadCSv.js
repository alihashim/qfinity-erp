import React from 'react';
import CSVReader from 'react-csv-reader';
import ApiService from '../../../../services/apiService';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom';

const CsvFileReader = () => {
    let navigate = useNavigate();

    const categoryMapping = {
        "Breakfast": 1,
        "Lunch": 2,
        "Dinner": 3,
        "Brunch": 4,
    };

    const SubmitData = async (csvData) => {
        try {
            const transformedData = transformCSVData(csvData);
            debugger
            const apiService = new ApiService();
            const response = await apiService.post('CREATE_BULK_MENU', transformedData);

            console.log('Response:', response.data);

            if (response.data.statusCode === 201) {
                toast.success('Menu items created successfully');
                navigate('/menu');
            } else {
                toast.warning('Waiting for menu update');
            }

        } catch (error) {
            console.error('Error:', error);
        }
    };

    const handleFile = async (data, fileInfo) => {
        try {
            SubmitData(data.slice(1)); // Skip the header row and pass data to SubmitData
        } catch (error) {
            console.error('Error:', error);
        }
    };

    const transformCSVData = csvData => {
        // Extract the category names from the first row of the CSV
        const categoryNames = csvData[0];

        // Map category names to category IDs
        const categoryIds = categoryNames?.map(name => categoryMapping[name]);

        // Transform each row of the CSV data into the required format for the API
        const menuItems = csvData.slice(1).map((item) => {
            const [category, itemName, description, price, ...addOns] = item;

            // Extract add-on names and prices in pairs
            const addOnPairs = [];
            for (let i = 0; i < addOns.length; i += 2) {
                const addOnName = addOns[i];
                const addOnPrice = addOns[i + 1];
                if (addOnName && addOnPrice) {
                    addOnPairs.push({
                        name: addOnName,
                        price: parseInt(addOnPrice, 10),
                    });
                }
            }

            return {
                name: itemName,
                description: description,
                price: parseInt(price, 10),
                categoryId: parseInt(categoryIds, 10),
                imageUrl: "string",
                addOns: addOnPairs,
            };
        });

        return { menuItems };
    };




    return (
        <div className=''>
            <div className='pb-3 chfile'>Choose File To Add Menu</div>
            <div className='row'>
                <div>
                    <CSVReader onFileLoaded={handleFile} />
                </div>
            </div>
        </div>
    );
};

export default CsvFileReader;