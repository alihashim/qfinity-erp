import { Draggable } from "react-beautiful-dnd";
import React from "react";
import styled from "styled-components";
import profileImg from "../../../../assets/images/Spiderman.png"

const CardFooter = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const DragItem = styled.div`
  padding: 10px;
  border-radius: 6px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  background: white;
  margin: 0 0 8px 0;
  display: grid;
  grid-gap: 20px;
  flex-direction: column;
`;

// const lorem = new LoremIpsum();

const OrderCard = ({ item, index }) => {
  //   const randomHeader = useMemo(() => lorem.generateWords(5), []);
  
  return (
    <Draggable draggableId={item?.id} index={index}>
      {(provided, snapshot) => {
        return (
          <DragItem
            ref={provided.innerRef}
            snapshot={snapshot}
            {...provided.draggableProps}
            {...provided.dragHandleProps}>
            {/* <CardHeader></CardHeader> */}
            <div className="">
              <div className="d-flex gap-3">
                <div className="itmImg">
                  <img src={profileImg} alt="Profile" />
                </div>
                <div className="p-2">
                  <div className="borderbtm"></div>
                  <h6>Ali Hashim</h6>
                </div>
                <br />
              </div>
            </div>
            <div className="row px-4">
              <div className="col-6">
                <h6>Order Time</h6>
                <h6>Amount</h6>
                <h6>Payment</h6>
                <h6>Table</h6>
              </div>
              <div className="col-6 cardvlu">
                <h6>Oct 28, 2022</h6>
                <h6>$ 700</h6>
                <h6>Debit Card</h6>
                <h6>T-1</h6>
              </div>
            </div>
            <CardFooter>
              <button className="crd-btn">
                Dine-in
              </button>
            </CardFooter>
          </DragItem>
        );
      }}
    </Draggable>
  );
};

export default OrderCard;
