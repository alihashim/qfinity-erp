import { React, useState, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom';
import { ReactComponent as List } from '../../../assets/images/svgIcons/list.svg';
import { ReactComponent as Next } from '../../../assets/images/svgIcons/pagination_right.svg';
import { ReactComponent as Previous } from '../../../assets/images/svgIcons/pagination_left.svg';
import "./order.css"
import order from "./map"
import ApiService from '../../../services/apiService';
import moment from 'moment/moment';

const Order = () => {
  const [orders, setorders] = useState([]);

  useEffect(() => {
    GetOrders();
  }, []);
  let navigate = useNavigate();
  const handleDND = () => {
    navigate('/ordersDND');
  }
  const singleItem = (e) => {
    
    navigate(`/order/${e?.id}/${e?.customerName}`, { state: e });
  }
  const GetOrders = async () => {
    const apiService = new ApiService();
    let res = await apiService.get("GET_ORDERS");
    setorders(res?.data?.result)
  }

  return (
    <div className="container-fluid m-0 p-0 p-lg-5 pad-top">
      <div className='d-flex gap-4'>
        <input type="search" class="form-control w-25" placeholder="Search" />
        <List className='filter' onClick={handleDND} />
      </div>
      <div className='row'>
        <div className='py-5 col-lg-12 '>
          <div className='table-responsive'>
            <table class="table table-hover" >
              <thead>
                <tr className='tHead'>
                  <th >Order ID</th>
                  <th >Date</th>
                  <th >Customer Name</th>
                  <th >Order Type</th>
                  <th >Order Source</th>
                  <th >Amount</th>
                  <th >Status Order</th>
                </tr>
              </thead>
              <tbody >
                {orders?.map((val) => {
                  return (
                    <tr className='ttt tbl-row trow'>
                      <td class="align-middle" onClick={() => singleItem(val)}>#  {val?.id}</td>
                      <td class="align-middle">{moment(val?.date).format('YYYY-MM-DD HH:mm:ss')}</td>
                      <td class="align-middle">{val?.customerName}</td>
                      <td class="align-middle">{val?.orderType}</td>
                      <td class="align-middle">{val?.orderSource}</td>
                      <td class="align-middle">$  {val?.orderAmount}</td>
                      <td>
                        <button className={val?.orderStatus === 'New Order' ? 'status-btn' : val?.orderStatus === 'On Delivery' ? 'status-delivery' : 'status-delivered'}>
                          {val?.orderStatus}
                        </button>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div className='d-flex justify-content-end'>
        <ul class="pagination">
          <li class="page-item"><a class="page-link paginationPrv" href="#"><Previous />  Previous  </a></li>
          <li class="page-item"><a class="page-link pag" href="#">1</a></li>
          <li class="page-item"><a class="page-link pag" href="#">2</a></li>
          <li class="page-item"><a class="page-link pag" href="#">3</a></li>
          <li class="page-item"><a class="page-link pag" href="#">4</a></li>
          <li class="page-item"><a class="page-link paginationPrv" href="#">Next <Next /></a></li>
        </ul>
      </div>
    </div>
  )
}

export default Order