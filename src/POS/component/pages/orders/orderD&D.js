import React from 'react'
import { useNavigate } from 'react-router-dom';
import "./order.css"
import { ReactComponent as Grid } from '../../../assets/images/svgIcons/Grid.svg';
import DragDrop from './D&D/drag&Drop';

const OrderDragNDrop = () => {
    let navigate = useNavigate();
    const handleDND = () => {
        navigate('/orders');
    }
    return (
        <div className="container-fluid m-0 p-0 p-lg-5 pad-top">
            <div className='d-flex gap-4'>
                <input type="search" class="form-control w-25" placeholder="Search" />
                <Grid className='filter' onClick={handleDND} />
            </div>
            <div className='py-5 DnDOvrflow'>
                <DragDrop />
            </div>
        </div>
    )
}

export default OrderDragNDrop