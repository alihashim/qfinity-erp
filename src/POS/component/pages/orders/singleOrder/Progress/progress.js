

import React from "react";
import "react-step-progress-bar/styles.css";
import { ProgressBar, Step } from "react-step-progress-bar"; import "./progress.css"
const Progress = ({val}) => {


    return (
        <ProgressBar
            percent={24}
            filledBackground="linear-gradient(to right, #D17E06, #D17E06)"
        >
            <Step transition="scale">
                {({ accomplished }) => (
                    <div className="txtali">
                        <div className="maintxt"> Order Confirmed</div>
                        <div className="py-2">
                            <div className="circle"></div>
                        </div>
                        <div className="smltxt">Wed, 1 lth Jan, 11:20 AM</div>
                    </div>
                )}
            </Step>
            <Step transition="scale">
                {({ accomplished }) => (
                    <div className="txtali">
                        <div className="maintxt">Cooking</div>
                        <div className="py-2">
                            <div className="circle"></div>
                        </div>
                        <div className="smltxt">Wed, 1 lth Jan, 11:20 AM</div>
                    </div>
                )}
            </Step>
            <Step transition="scale">
                {({ accomplished }) => (
                    <div className="txtali">
                        <div className="maintxt">On the way</div>
                        <div className="py-2">
                            <div className="circle"></div>
                        </div>
                        <div className="smltxt">Wed, 1 lth Jan, 11:20 AM</div>
                    </div>
                )}
            </Step>
            <Step transition="scale">
                {({ accomplished }) => (
                    <div className="txtali">
                        <div className="maintxt">Delivered </div>
                        <div className="circle"></div>
                        <div className="smltxt">Wed, 1 lth Jan, 11:20 AM</div>
                    </div>
                )}
            </Step>
        </ProgressBar>
    );
};

export default Progress;
