import Pizza from "../../../../assets/images/P2.png"
import Pizza2 from "../../../../assets/images/P-3.png"

export const OrderItem = [
    {
        Id: 1,
        imageUrl: Pizza,
        ItemName: "Crunch Pizza",
        price: "$ 250",
        qty: "1",
        adOn: "Extra  Cheese",
        adOn2: "Extra Tooping Jalapeno ",
        ItemSize: "Medium",
        KitchenInstruction: "Kitchen Instruction "
    },
    {
        Id: 2,
        imageUrl: Pizza2,
        ItemName: "Crunch Pizza",
        price: "$ 250",
        qty: "3",
        adOn: "Extra  Cheese",
        adOn2: "Extra Tooping Jalapeno ",
        ItemSize: "Medium",
        KitchenInstruction: "Kitchen Instruction "
    },
    {
        Id: 3,
        imageUrl: Pizza,
        ItemName: "Crunch Pizza",
        price: "$ 250",
        qty: "1",
        adOn: "Extra  Cheese",
        adOn2: "Extra Tooping Jalapeno ",
        ItemSize: "Medium",
        KitchenInstruction: "Kitchen Instruction "
    },

];
export default OrderItem;