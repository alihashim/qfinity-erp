import React from 'react';
import "../singleOrder.css"
import "./item.css"
import img  from "../../../../../assets/images/Item-3.png"
const SingleOrderItem = ({ val }) => {


    return (
        <div className='p-3'>
            <div className='row'>
                <div className='col-lg-1 end'>
                    <img src={img} className='pikitm' />
                </div>
                <div className='col-lg-10'>
                    <div className='itmnme'>{val?.itemName}</div>
                    <div className='itmKIns'>{val?.addOns?.[0]?.name} | {val?.kitchenInstructions}</div>
                </div>
                <div className='col-lg-1 '>
                    <div>
                        <div className='itmprc'>$ {val?.amount}</div>
                        <div className='itmKIns'>{val?.quantity}</div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SingleOrderItem;
