import React from 'react';
import "../singleOrder.css"

const PaySection = ({val}) => {
    

    return (
        <div className='d-flex justify-content-between'>
            <div>
                <div className='itmmain'>Payment</div>
                <div className='carddet'>Visa **56</div>
                <div className='carddet'>{val?.customerName}</div>

            </div>
            <div className=''>
                <div className='itmmain end'>Delivery</div>
                <div className='end carddet'>Address</div>
                <div className='itmtitl'>{val?.orderType}</div>
            </div>
        </div>
    );
};

export default PaySection;
