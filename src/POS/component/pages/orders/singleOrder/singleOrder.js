import React, { useEffect, useState } from 'react';
import "./singleOrder.css"
import AdminHeader from '../../header/adminHeader';
import Progress from './Progress/progress';
import SingleOrderItem from './item/item';
import OrderItem from './data';
import PaySection from './paymentSection/paymentSection';
import { useLocation, useNavigate } from 'react-router-dom';
import ApiService from '../../../../services/apiService';
import order from '../map';
const SingleOrder = () => {

    const [orderDetails, setorders] = useState([]);
    const { state } = useLocation();
    const Itemdata = state

    useEffect(() => {
        getItemDetails();
    }, []);
    let navigate = useNavigate();


    const getItemDetails = async () => {
        const apiService = new ApiService();
        let paramaters = `${Itemdata?.id}`;
        let res = await apiService.getApiParamater("GET_ITEM_DETAILS", paramaters);
        setorders(res?.data?.result)
        
    }

    return (
        <div className='p-lg-5'>
            <AdminHeader />
            <div className='p-lg-5'>
                <div className='p-lg-5'>
                    <Progress val={orderDetails} />
                </div>
                <div className='py-lg-3'>
                    {orderDetails?.orderItems?.map((val, key) => (
                        <SingleOrderItem val={val} />
                    ))}
                </div>
                <div>
                    <PaySection val={orderDetails}/>
                </div>
                <div >
                    <div className='itmmain'>Order Summary</div>
                    <div className='d-flex justify-content-between'>
                        <div className='itmtitl'>Discount</div>
                        <div className='itmtitl'>{orderDetails?.totalDiscount}</div>
                    </div>
                    {/* <div className='d-flex justify-content-between'>
                        <div className='itmtitl'>Delivery</div>
                        <div className='itmtitl'>$0.00</div>
                    </div> */}
                    <div className='d-flex justify-content-between'>
                        <div className='itmtitl'>Tax</div>
                        <div className='itmtitl'>+$ {orderDetails?.tax}</div>
                    </div>
                    <div className='d-flex justify-content-between'>
                        <div className='itmtitl'>Total</div>
                        <div className='itmpay'>$ {orderDetails?.orderAmount || "650"}</div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SingleOrder;
