import React, { useState } from 'react';
import "./homes.css";
import logo from "../../assets/images/Logo.png";
import Dropdown from './dropDowns/dropDown';
import { Link } from 'react-router-dom';

const Splash = () => {
    const [activeButton, setActiveButton] = useState('DELIVERY');

    const handleButtonClick = (buttonName) => {
        setActiveButton(buttonName);
    };

    return (
        <div className='container-fluid bg-main'>
            <div className='row'>
                <div className='col inner-body p-3'>
                    <div className='padds'>
                        <div>
                            <img src={logo} alt="Logo" />
                        </div>
                        <div className='ord-in-que mt-4 row justify-content-center'>
                            Select your order type
                        </div>
                        <div className='row mt-3 justify-content-center mb-3'>
                            <div
                                className={`col-lg-3 col-md-3  button ${activeButton === 'DELIVERY' ? 'active' : ''}`}
                                onClick={() => handleButtonClick('DELIVERY')}
                            >
                                DELIVERY
                            </div>
                            <div
                                className={`col-lg-3 col-md-3 button ${activeButton === 'PICKUP' ? 'active' : ''}`}
                                onClick={() => handleButtonClick('PICKUP')}
                            >
                                PICKUP
                            </div>
                            <div
                                className={`col-lg-3 col-md-3 button ${activeButton === 'DINE IN' ? 'active' : ''}`}
                                onClick={() => handleButtonClick('DINE IN')}
                            >
                                DINE IN
                            </div>
                        </div>
                        <div className='ord-in-que mt-4 mb-4 row'>
                            Which outlet would you like to pick-up from?
                        </div>
                        <Dropdown />
                        <div className='row justify-content-center mt-5'>
                            <Link className='decor_n justify-content-center d-flex' to="/costumer/menu">
                            <div  className='letsGo-btn col-lg-6 col-md-6'>
                                Let's Order
                            </div>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Splash;
