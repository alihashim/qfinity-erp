import React, { useState } from 'react';
import Sider from '../../../assets/images/LoginSider.png';
import Logo from '../../../assets/images/Logo.png';
import google from '../../../assets/images/Google.png';
import facebook from '../../../assets/images/Facebook.png';
import './auth.css';
import { Link, useNavigate } from 'react-router-dom';
import ApiService from '../../../services/apiService'; // Import the ApiService class
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Login = () => {
    let navigate = useNavigate();
    const [formData, setFormData] = useState({
        emailOrPhone: '',
        password: '',
    });

    const handleChange = (event) => {
        const { name, value } = event.target;
        setFormData({
            ...formData,
            [name]: value,
        });
    };

    const handleLogin = async () => {
        const apiService = new ApiService();
        let body = {
          email: formData.emailOrPhone,
          password: formData.password,
        };
      
        try {
          // Call the login API
          const response = await apiService.post('USER_LOGIN', body);
      
          if (response && response.status === 200) {
            // Update the token in local storage
            apiService.setToken(response?.data?.result?.token);
            apiService.setRefreshToken(response?.data?.result?.refreshToken);

      
            toast.success('Login successful!', {
              position: 'top-right',
              autoClose: 3000,
            });
      
            navigate('/menu');
          } else if (response && response.status === 400) {
            for (const error of response.data.errors) {
              toast.error(error, {
                position: 'top-right',
                autoClose: 5000,
              });
            }
          } else {
            toast.error(`Login failed: ${response ? response.data.message : 'Unknown error'}`, {
              position: 'top-right',
              autoClose: 5000,
            });
          }
        } catch (error) {
          if (error.response && error.response.status === 401) {
            // Unauthorized error
            toast.error('Unauthorized. Please check your credentials.', {
              position: 'top-right',
              autoClose: 5000,
            });
          } else {
            // Other errors
            toast.error('Login failed', {
              position: 'top-right',
              autoClose: 5000,
            });
          }
        }
      };
      
    


    return (
        <div className="container-fluid display-flex body m-0 p-0">
            <div className="row">
                <div className="col-lg-6 m-0 col-sm-12">
                    <img src={Sider} className="sider_login" alt="Login Sider" />
                </div>
                <div className="col-lg-6 m-0 p-5 col-sm-12">
                    <div className="row">
                        <img src={Logo} className="logo_size" alt="Logo" />
                    </div>
                    <div className="p-lg-5 p-sm-0 p-md-0">
                        <div className="login_head p-3">Welcome Back to Qfinity</div>

                        <form className="">
                            <div className="form-group p-3">
                                <label className="pb-2 labels" htmlFor="emailOrPhone">
                                    Email or phone number
                                </label>
                                <input
                                    type="text"
                                    className="form-control p-2"
                                    id="emailOrPhone"
                                    name="emailOrPhone"
                                    value={formData.emailOrPhone}
                                    onChange={handleChange}
                                />
                            </div>
                            <div className="form-group p-3">
                                <label className="pb-2 labels" htmlFor="password">
                                    Password
                                </label>
                                <input
                                    type="password"
                                    className="form-control p-2"
                                    id="password"
                                    name="password"
                                    value={formData.password}
                                    onChange={handleChange}
                                />
                            </div>
                        </form>

                        <div className="align_center mt-5">
                            <div className="form-group p-3 d-flex justify-content-center mt-3 align_center">
                                <button
                                    onClick={handleLogin}
                                    className="btn Button_Normal center p-2"
                                >
                                    Login
                                </button>
                            </div>
                            <p className="simple_mini m-0">
                                Don’t have an account?
                                <Link to="/Signup" className="orange_text">
                                    Sign Up
                                </Link>
                            </p>
                        </div>

                        <div className="row p-4 align_center">
                            <span className="head2">OR</span>
                        </div>

                        <div className='row align_center'>
                            <div className='col-lg-5'>
                                <div className='border_simple'>
                                    <img src={google} className='icons' />
                                    <span className='Auth_Btn'>
                                        Log In with Google
                                    </span>
                                </div>
                            </div>
                            <div className='col-lg-5'>
                                <div className='border_simple'>
                                    <img src={facebook} className='icons' />
                                    <span className='Auth_Btn'>
                                        Log In with Facebook
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;
