const BASEURL = process.env.REACT_APP_BACKEND_URL

export const endPoint = {
    USER_REGISTER: `http://51.104.48.182/api/Account/Register`,
    USER_LOGIN: `http://51.104.48.182/api/Account/Login`,
    REFRESH_TOKEN: `http://51.104.48.182/api/Account/Refresh`,
    CREATE_MENU: `${BASEURL}/api/MenuItem/Create`,
    CREATE_BULK_MENU: `${BASEURL}/api/MenuItem/CreateInBulk`,
    CREATE_CATEGORY: `${BASEURL}/api/Category/Create`,
    GET_CATEGORY: `${BASEURL}/api/Category/GetList`,
    GET_MENU: `${BASEURL}/api/MenuItem/GetList`,
    CREATE_ORDER: `${BASEURL}/Order/Create`,
    GET_ORDERS: `${BASEURL}/Order/GetList`,
    GET_ITEM_DETAILS: `${BASEURL}/Order/GetById/`,
    DELETE_ITEM: `${BASEURL}/api/MenuItem/Delete/`,
    ORDER_SUMMERY: `${BASEURL}/api/Dashboard/GetOrderSummery`,
    ORDER_STATUS: `${BASEURL}/Order/UpdateStatus`,
}