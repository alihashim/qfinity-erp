import axios from "axios";
import { endPoint } from "../constants/endPoints";

const defaultHeaders = {
  // 'Accept': 'application/json',
  'Content-Type': 'application/json',
};

let headersForm = {
  "Content-Type": "multipart/form-data",
}


class ApiService {
  constructor() {
    // Initialize Axios instance with default headers
    this.axiosInstance = axios.create({
      baseURL: "http://51.104.48.182", // Replace with your API base URL
      headers: defaultHeaders,
    });

    // Set up request and response interceptors
    this.axiosInstance.interceptors.request.use(this.requestInterceptor);
    this.axiosInstance.interceptors.response.use(this.responseInterceptor, this.errorInterceptor);
  }

  // Request interceptor to add authorization header
  requestInterceptor = (config) => {
    const token = this.getToken();
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`;
    }
    return config;
  };

  // Response interceptor to handle token refresh and other actions
  responseInterceptor = (response) => {
    return response;
  };

  // Error interceptor to handle token refresh and other actions
  errorInterceptor = async (error) => {
    if (error.response && error.response.status === 401) {
      const token = this.getToken();
      if (token) {
        // Handle token refresh for requests with a token
        await this.handleRefreshToken();
        const newToken = this.getToken();
        if (newToken) {
          error.config.headers['Authorization'] = `Bearer ${newToken}`;
          return axios.request(error.config);
        } else {
          console.error('Token refresh failed');
          return Promise.reject(error);
        }
      } else if (this.isLoginOrSignupRequest(error.config)) {
        // For the first-time login or signup request without a token
        return axios.request(error.config);
      } else {
        console.error('User not authenticated');
        // You can add your logic here, like redirecting to a login page
        return Promise.reject(new Error('Unauthorized'));
      }
    } else {
      return Promise.reject(error);
    }
  };

  // Check if a request is for login or signup based on the URL
  isLoginOrSignupRequest(config) {
    const url = config.url;
    // Check if the URL contains login or signup keywords
    return url.includes('USER_LOGIN') || url.includes('USER_SIGNUP');
  }

  async handleRefreshToken() {
    // Implement the logic to refresh the token here
    // You can make a POST request to your token refresh endpoint
    // and update the token in the local storage
    try {
      // Example: Call the refresh token API
      const response = await this.axiosInstance.post(endPoint['REFRESH_TOKEN'], {
        token: this.getToken(),
        refreshToken: this.getRefreshToken(), // Get the refresh token from local storage
      });

      if (response.status === 200) {
        // Update the token in local storage
        this.setToken(response.data.result.token);
      }
    } catch (error) {
      console.error('Token refresh failed', error);
    }
  }

  getToken() {
    // Retrieve the token from local storage
    return localStorage.getItem('Token');
  }

  setToken(token) {
    // Update the token in local storage
    localStorage.setItem('Token', token);
  }

  getRefreshToken() {
    // Retrieve the refresh token from local storage
    return localStorage.getItem('RefreshToken');
  }

  setRefreshToken(refreshToken) {
    // Update the refresh token in local storage
    localStorage.setItem('RefreshToken', refreshToken);
  }

  async get(path) {
    return this.axiosInstance.get(endPoint[path]);
  }
  async getApiParamater(path, data) {
    return this.axiosInstance.get(endPoint[path] + data);
  }
  async post(path, data) {
    return this.axiosInstance.post(endPoint[path], data);
  }

  async put(path, data) {
    return this.axiosInstance.put(endPoint[path], data);
  }

  async delete(path, data) {
    return this.axiosInstance.delete(endPoint[path] + data);
  }
}

export default ApiService;

