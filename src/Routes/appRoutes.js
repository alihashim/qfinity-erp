import React from 'react';
import '../POS/component/pages/sidebars.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
// import Home from '../POS/component/pages/home';
import Menu from '../POS/component/pages/menus/menu';
import Orders from '../POS/component/pages/orders/order';
import Login from '../POS/component/pages/user/login';
import CashierRoutes from '../POS/Cashier/CashierRouter';
import Signup from '../POS/component/pages/user/signup';
import OrderDragNDrop from '../POS/component/pages/orders/orderD&D';
import CustomerMenu from '../POS/component/pages/Costumer/Menu/customerMenu';
import AddItem from '../POS/component/pages/menus/addItems';
import SingleItem from '../POS/component/pages/menus/singleItem';
import Splash from '../POS/component/pages/splash';
import AddtoCart from '../POS/component/pages/Costumer/Cart/cart';
import MainLayout from '../POS/component/pages/SideBar/mainLayout';
import PlainLayout from '../POS/component/pages/SideBar/plainLayout';
import CardDetails from '../POS/component/pages/Costumer/Payment/cardDetails';
import Dashboard from '../POS/component/pages/Dashboard/dashboard';
import SingleOrder from '../POS/component/pages/orders/singleOrder/singleOrder';
import WaitPage from '../POS/component/pages/Costumer/Payment/waitPage';
import KitchenDisplay from '../POS/KDS/kitchenDisplay';
const AppRoutes = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<PlainLayout><Login /></PlainLayout>} />
        <Route path="/" element={<PlainLayout><Splash /></PlainLayout>} />
        <Route path="/signup" element={<PlainLayout><Signup /></PlainLayout>} />
        <Route path="/costumer/menu" element={<PlainLayout><CustomerMenu /></PlainLayout>} />
        <Route path="/costumer/menu/Cart" element={<PlainLayout><AddtoCart /></PlainLayout>} />
        <Route path="/costumer/menu/Cart/Payment" element={<PlainLayout><CardDetails /></PlainLayout>} />
        <Route path="/costumer/wait" element={<PlainLayout><WaitPage /></PlainLayout>} />
        <Route path="/cashier/*" element={<CashierRoutes />} />
        <Route path="/kitchen/*" element={<KitchenDisplay />} />
        
        {/* Apply Sidebar to specific pages */}
        <Route path="/menu" element={<MainLayout><Menu /></MainLayout>} />
        <Route path="/orders" element={<MainLayout><Orders /></MainLayout>} />
        <Route path="/order/:id/:name" element={<MainLayout><SingleOrder /></MainLayout>} />
        {/* <Route path="/Home" element={<MainLayout><Home /></MainLayout>} /> */}
        <Route path="/Home" element={<MainLayout><Dashboard /></MainLayout>} />
        <Route path="/ordersDND" element={<MainLayout><OrderDragNDrop /></MainLayout>} />
        <Route path="/additems" element={<MainLayout><AddItem /></MainLayout>} />
        <Route path="/singleMenu" element={<MainLayout><SingleItem /></MainLayout>} />
      </Routes>
    </BrowserRouter>
  );
};

export default AppRoutes;
